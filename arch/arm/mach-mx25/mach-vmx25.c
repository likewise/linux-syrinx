/*
 * arch/arm/mach-mx25/mach-vmx25.c
 *
 * Copyright (C) 2010  Voipac <support@voipac.com>
 *
 * Based on arch/arm/mach-mx25/karo-tx25.c 
 * Copyright (C) 2009  Lothar Wassmann <LW@KARO-electronics.de>
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the:
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301
 *
 * This file adds support for the Voipac VMX25 processor modules
 */

#include <linux/types.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/init.h>
#include <linux/ioport.h>
#include <linux/platform_device.h>
#include <linux/input.h>
#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/fb.h>
#include <linux/serial_8250.h>
#include <linux/fec.h>
#if defined(CONFIG_MTD) || defined(CONFIG_MTD_MODULE)
#include <mtd/mtd-abi.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <asm/mach/flash.h>
#endif

#include <linux/serial.h>
#include <linux/fsl_devices.h>
#include <linux/irq.h>
#include <linux/mmc/host.h>
#include <linux/leds.h>
#include <linux/if_ether.h>

#include <asm/setup.h>
#include <asm/irq.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/time.h>
#include <mach/common.h>
#include <mach/hardware.h>
#include <mach/gpio.h>
#include <mach/iomux-v1.h>
#include <mach/iomux-v3.h>
#include <mach/iomux-mx25.h>
#include <mach/irqs.h>
#include <mach/clock.h>
#include <mach/imxfb.h>
#include <mach/mmc.h>
#include <mach/mxc_nand.h>
#if defined(CONFIG_TOUCHSCREEN_MXC_TSC) || defined(CONFIG_TOUCHSCREEN_MXC_TSC_MODULE)
#include <mach/mxc_tsc.h>
#endif

#include "devices.h"

int vmx_mod_type = -1;

static void vmx25_gpio_config(struct pad_desc *pd, int num)
{
	int i;

	for (i = 0; i < num; i++) {
		if (mxc_iomux_v3_setup_pad(&pd[i]) == 0) {
			mxc_iomux_v3_release_pad(&pd[i]);
		}
	}
}

/* MTD NAND flash */
#if defined(CONFIG_MTD_NAND_MXC) || defined(CONFIG_MTD_NAND_MXC_MODULE)
static struct pad_desc vmx25_nand_pads[] = {
	MX25_PAD_NF_CE0__NF_CE0,
	MX25_PAD_NFWE_B__NFWE_B,
	MX25_PAD_NFRE_B__NFRE_B,
	MX25_PAD_NFALE__NFALE,
	MX25_PAD_NFCLE__NFCLE,
	MX25_PAD_NFWP_B__NFWP_B,
	MX25_PAD_NFRB__NFRB,
	MX25_PAD_D7__D7,
	MX25_PAD_D6__D6,
	MX25_PAD_D5__D5,
	MX25_PAD_D4__D4,
	MX25_PAD_D3__D3,
	MX25_PAD_D2__D2,
	MX25_PAD_D1__D1,
	MX25_PAD_D0__D0,
};

static struct mtd_partition mxc_nand_partitions[] = {
	{
	 .name = "nand.barebox",
	 .offset = 0,
	 .size = 256 * 1024},
	{
	 .name = "nand.bareboxenv",
	 .offset = MTDPART_OFS_APPEND,
	 .size = 128 * 1024},
	{
	 .name = "nand.kernel",
	 .offset = MTDPART_OFS_APPEND,
	 .size = 2688 * 1024},
	{
	 .name = "nand.rootfs",
	 .offset = MTDPART_OFS_APPEND,
	 .size = MTDPART_SIZ_FULL},
};

static struct mxc_nand_platform_data vmx25_nand_pdata = {
	.parts = mxc_nand_partitions,
	.nr_parts = ARRAY_SIZE(mxc_nand_partitions),
	.hw_ecc		= 1,
	.width		= 1,
	.flash_bbt	= 1,
};

static int vmx25_nand_init(void)
{
	int ret;

	printk(KERN_INFO "%s: Configuring NAND pins\n", __FUNCTION__);
	ret = mxc_iomux_v3_setup_multiple_pads(vmx25_nand_pads,
					       ARRAY_SIZE(vmx25_nand_pads));

	return ret;
}
arch_initcall(vmx25_nand_init);
#endif // CONFIG_MTD_NAND_MXC CONFIG_MTD_NAND_MXC_MODULE

#if defined(CONFIG_FEC) || defined(CONFIG_FEC_MODULE)

static struct fec_platform_data vmx25_fec_pdata = {
        .phy    = PHY_INTERFACE_MODE_RMII,
};

/*
 * Setup GPIO for FEC device
 *
 */

static struct pad_desc vmx25_fec_pwr_gpios[] = {
	MX25_PAD_D11__GPIO_4_9,		/* FEC PHY power on pin */
	MX25_PAD_D13__GPIO_4_7,		/* FEC reset */
};

#define VMX25_FEC_PWR_GPIO	(GPIO_PORTD | 9)
#define VMX25_FEC_RST_GPIO	(GPIO_PORTD | 7)

static struct pad_desc vmx25_fec_gpios[] = {
	MX25_PAD_FEC_MDC__FEC_MDC,
	MX25_PAD_FEC_MDIO__FEC_MDIO,
	MX25_PAD_FEC_TDATA0__FEC_TDATA0,
	MX25_PAD_FEC_TDATA1__FEC_TDATA1,
	MX25_PAD_FEC_TX_EN__FEC_TX_EN,
	MX25_PAD_FEC_RDATA0__FEC_RDATA0,
	MX25_PAD_FEC_RDATA1__FEC_RDATA1,
	MX25_PAD_FEC_RX_DV__FEC_RX_DV,
	MX25_PAD_FEC_TX_CLK__FEC_TX_CLK,
};

static int vmx25_fec_init(void)
{
	int ret;

	printk(KERN_INFO "%s: Configuring FEC pins\n", __FUNCTION__);
	ret = mxc_iomux_v3_setup_multiple_pads(vmx25_fec_pwr_gpios,
					       ARRAY_SIZE(vmx25_fec_pwr_gpios));
	if (ret) {
		return ret;
	}

	gpio_request(VMX25_FEC_PWR_GPIO, "FEC PHY enable");
	gpio_request(VMX25_FEC_RST_GPIO, "FEC PHY reset");


	/* turn off PHY power and lift reset */
	gpio_direction_output(VMX25_FEC_PWR_GPIO, 0); /* drop PHY power */
	gpio_direction_output(VMX25_FEC_RST_GPIO, 0); /* assert reset */

	ret = mxc_iomux_v3_setup_multiple_pads(vmx25_fec_gpios,
						       ARRAY_SIZE(vmx25_fec_gpios));

	udelay(100);

	/* turn on PHY power and lift reset */
	gpio_set_value(VMX25_FEC_PWR_GPIO, 1);
	gpio_set_value(VMX25_FEC_RST_GPIO, 1);

	return ret;
}
arch_initcall(vmx25_fec_init);

#endif // CONFIG_FEC CONFIG_FEC_MODULE

#if defined(CONFIG_TOUCHSCREEN_MXC_TSC) || defined(CONFIG_TOUCHSCREEN_MXC_TSC_MODULE)
static struct mxc_tsc_pdata vmx25_tsc_pdata = {
	.pen_debounce_time = 32,
	.intref = 1,
	.adc_clk = 1666667,
	.tsc_mode = MXC_TSC_4WIRE,
	.r_xplate = 660,
	.hsyncen = 1,
	.hsyncpol = 0,
};
#endif

static struct platform_dev_list {
	struct platform_device *pdev;
	void *pdata;
} vmx25_devices[] __initdata = {
#if defined(CONFIG_IMX2_WDT) || defined(CONFIG_IMX2_WDT_MODULE)
	{ .pdev = &mxc_wdt, },
#endif
#if defined(CONFIG_RTC_DRV_IMXDI) || defined(CONFIG_RTC_DRV_IMXDI_MODULE)
	{ .pdev = &mx25_rtc_device, },
#endif
#if defined(CONFIG_MTD_NAND_MXC) || defined(CONFIG_MTD_NAND_MXC_MODULE)
	{ .pdev = &mxc_nand_device, &vmx25_nand_pdata},
#endif
#if defined(CONFIG_FEC) || defined(CONFIG_FEC_MODULE)
	{ .pdev = &mx25_fec_device, &vmx25_fec_pdata},
#endif
#if defined(CONFIG_IMX_ADC) || defined(CONFIG_IMX_ADC_MODULE)
	{ .pdev = &imx_adc_device, },
#endif
#if defined(CONFIG_TOUCHSCREEN_MXC_TSC) || defined(CONFIG_TOUCHSCREEN_MXC_TSC_MODULE)
	{ .pdev = &mx25_tsc_device, &vmx25_tsc_pdata, },
#endif
#if defined(CONFIG_W1_MASTER_MXC) || defined(CONFIG_W1_MASTER_MXC_MODULE)
	{ .pdev = &mxc_w1_master_device, },
#endif
};
#define VMX25_NUM_DEVICES		ARRAY_SIZE(vmx25_devices)

static __init void vmx25_board_init(void)
{
	int i;

	printk(KERN_INFO "%s: \n", __FUNCTION__);

	for (i = 0; i < VMX25_NUM_DEVICES; i++) {
		int ret;

		if (vmx25_devices[i].pdev == NULL) continue;

		printk(KERN_INFO "%s: Registering platform device[%d] @ %p dev %p: %s\n",
		    __FUNCTION__, i, vmx25_devices[i].pdev, &vmx25_devices[i].pdev->dev,
		    vmx25_devices[i].pdev->name);
		if (vmx25_devices[i].pdata) {
			ret = mxc_register_device(vmx25_devices[i].pdev,
						vmx25_devices[i].pdata);
		} else {
			ret = platform_device_register(vmx25_devices[i].pdev);
		}
		if (ret) {
			printk(KERN_WARNING "%s: Failed to register platform_device[%d]: %s: %d\n",
			       __FUNCTION__, i, vmx25_devices[i].pdev->name, ret);
		}
	}
#if defined(CONFIG_RTC_DRV_IMXDI) || defined(CONFIG_RTC_DRV_IMXDI_MODULE)
	device_init_wakeup(&mx25_rtc_device.dev, 1);
#endif

	printk(KERN_INFO "%s: Done\n", __FUNCTION__);
}

static struct pad_desc vmx25_gpios[] __refdata = {
	MX25_PAD_GPIO_A__GPIO_A,
	MX25_PAD_GPIO_B__GPIO_B,
	MX25_PAD_GPIO_C__GPIO_C,
	MX25_PAD_GPIO_D__GPIO_D,
	MX25_PAD_GPIO_E__GPIO_E,
	MX25_PAD_GPIO_F__GPIO_F,
	MX25_PAD_CSI_D7__GPIO_1_6,
	MX25_PAD_CSI_D8__GPIO_1_7,
	MX25_PAD_CSI_MCLK__GPIO_1_8,
	MX25_PAD_CSI_VSYNC__GPIO_1_9,
	MX25_PAD_CSI_HSYNC__GPIO_1_10,
	MX25_PAD_CSI_PIXCLK__GPIO_1_11,
	MX25_PAD_I2C1_CLK__GPIO_1_12,
	MX25_PAD_I2C1_DAT__GPIO_1_13,
	MX25_PAD_CSPI1_MOSI__GPIO_1_14,
	MX25_PAD_CSPI1_MISO__GPIO_1_15,
	MX25_PAD_CSPI1_SS0__GPIO_1_16,
	MX25_PAD_CSPI1_SS1__GPIO_1_17,
	MX25_PAD_CSPI1_SCLK__GPIO_1_18,
	MX25_PAD_LD5__GPIO_1_19,
	MX25_PAD_LD6__GPIO_1_20,
	MX25_PAD_LD7__GPIO_1_21,
	MX25_PAD_HSYNC__GPIO_1_22,
	MX25_PAD_VSYNC__GPIO_1_23,
	MX25_PAD_LSCLK__GPIO_1_24,
	MX25_PAD_OE_ACD__GPIO_1_25,
	MX25_PAD_PWM__GPIO_1_26,
	MX25_PAD_CSI_D2__GPIO_1_27,
	MX25_PAD_CSI_D3__GPIO_1_28,
	MX25_PAD_CSI_D4__GPIO_1_29,
	MX25_PAD_CSI_D5__GPIO_1_30,
	MX25_PAD_CSI_D6__GPIO_1_31,
	MX25_PAD_A14__GPIO_2_0,
	MX25_PAD_A15__GPIO_2_1,
	MX25_PAD_A16__GPIO_2_2,
	MX25_PAD_A17__GPIO_2_3,
	MX25_PAD_A18__GPIO_2_4,
	MX25_PAD_A19__GPIO_2_5,
	MX25_PAD_A20__GPIO_2_6,
	MX25_PAD_A21__GPIO_2_7,
	MX25_PAD_A22__GPIO_2_8,
	MX25_PAD_A23__GPIO_2_9,
	MX25_PAD_A24__GPIO_2_10,
	MX25_PAD_A25__GPIO_2_11,
	MX25_PAD_EB0__GPIO_2_12,
	MX25_PAD_EB1__GPIO_2_13,
	MX25_PAD_OE__GPIO_2_14,
	MX25_PAD_LD0__GPIO_2_15,
	MX25_PAD_LD1__GPIO_2_16,
	MX25_PAD_LD2__GPIO_2_17,
	MX25_PAD_LD3__GPIO_2_18,
	MX25_PAD_LD4__GPIO_2_19,
	MX25_PAD_DE_B__GPIO_2_20,
	MX25_PAD_CLKO__GPIO_2_21,
	MX25_PAD_CSPI1_RDY__GPIO_2_22,
#if 0  /* do not mess with these */
	MX25_PAD_SD1_CMD__GPIO_2_23,
	MX25_PAD_SD1_CLK__GPIO_2_24,
	MX25_PAD_SD1_DATA0__GPIO_2_25,
	MX25_PAD_SD1_DATA1__GPIO_2_26,
	MX25_PAD_SD1_DATA2__GPIO_2_27,
	MX25_PAD_SD1_DATA3__GPIO_2_28,
#endif
	MX25_PAD_KPP_ROW0__GPIO_2_29,
	MX25_PAD_KPP_ROW1__GPIO_2_30,
	MX25_PAD_KPP_ROW2__GPIO_2_31,
	MX25_PAD_KPP_ROW3__GPIO_3_0,
	MX25_PAD_KPP_COL0__GPIO_3_1,
	MX25_PAD_KPP_COL1__GPIO_3_2,
	MX25_PAD_KPP_COL2__GPIO_3_3,
	MX25_PAD_KPP_COL3__GPIO_3_4,
#if 0  /* do not mess with these */
	MX25_PAD_FEC_MDC__GPIO_3_5,
	MX25_PAD_FEC_MDIO__GPIO_3_6,
	MX25_PAD_FEC_TDATA0__GPIO_3_7,
	MX25_PAD_FEC_TDATA1__GPIO_3_8,
	MX25_PAD_FEC_TX_EN__GPIO_3_9,
	MX25_PAD_FEC_RDATA0__GPIO_3_10,
	MX25_PAD_FEC_RDATA1__GPIO_3_11,
	MX25_PAD_FEC_RX_DV__GPIO_3_12,
	MX25_PAD_FEC_TX_CLK__GPIO_3_13,
#endif
#if defined(CONFIG_W1_MASTER_MXC) || defined(CONFIG_W1_MASTER_MXC_MODULE)
	MX25_PAD_RTCK__OWIRE,
#else
	MX25_PAD_RTCK__GPIO_3_14,
#endif
	MX25_PAD_EXT_ARMCLK__GPIO_3_15,
	MX25_PAD_UPLL_BYPCLK__GPIO_3_16,
	MX25_PAD_VSTBY_REQ__GPIO_3_17,
	MX25_PAD_VSTBY_ACK__GPIO_3_18,
	MX25_PAD_POWER_FAIL__GPIO_3_19,
	MX25_PAD_CS4__GPIO_3_20,
	MX25_PAD_CS5__GPIO_3_21,
#if 0	/* do not mess with these */
	MX25_PAD_NF_CE0__GPIO_3_22,
#endif
	MX25_PAD_ECB__GPIO_3_23,
	MX25_PAD_LBA__GPIO_3_24,
	MX25_PAD_RW__GPIO_3_25,
#if 0	/* do not mess with these */
	MX25_PAD_NFWE_B__GPIO_3_26,
	MX25_PAD_NFRE_B__GPIO_3_27,
	MX25_PAD_NFALE__GPIO_3_28,
	MX25_PAD_NFCLE__GPIO_3_29,
	MX25_PAD_NFWP_B__GPIO_3_30,
	MX25_PAD_NFRB__GPIO_3_31,
#endif
	MX25_PAD_A10__GPIO_4_0,
	MX25_PAD_A13__GPIO_4_1,
	MX25_PAD_CS0__GPIO_4_2,
	MX25_PAD_CS1__GPIO_4_3,
	MX25_PAD_BCLK__GPIO_4_4,
	MX25_PAD_D15__GPIO_4_5,
	MX25_PAD_D14__GPIO_4_6,
	MX25_PAD_D13__GPIO_4_7,
	MX25_PAD_D12__GPIO_4_8,
	MX25_PAD_D11__GPIO_4_9,
	MX25_PAD_D10__GPIO_4_10,
	MX25_PAD_D9__GPIO_4_11,
	MX25_PAD_D8__GPIO_4_12,
#if 0	/* do not mess with these */
	MX25_PAD_D7__GPIO_4_13,
	MX25_PAD_D6__GPIO_4_14,
	MX25_PAD_D5__GPIO_4_15,
	MX25_PAD_D4__GPIO_4_16,
	MX25_PAD_D3__GPIO_4_17,
	MX25_PAD_D2__GPIO_4_18,
	MX25_PAD_D1__GPIO_4_19,
	MX25_PAD_D0__GPIO_4_20,
#endif
	MX25_PAD_CSI_D9__GPIO_4_21,
	MX25_PAD_UART1_RXD__GPIO_4_22,
	MX25_PAD_UART1_TXD__GPIO_4_23,
	MX25_PAD_UART1_RTS__GPIO_4_24,
	MX25_PAD_UART1_CTS__GPIO_4_25,
	MX25_PAD_UART2_RXD__GPIO_4_26,
	MX25_PAD_UART2_TXD__GPIO_4_27,
	MX25_PAD_UART2_RTS__GPIO_4_28,
	MX25_PAD_UART2_CTS__GPIO_4_29,
	MX25_PAD_BOOT_MODE0__GPIO_4_30,
	MX25_PAD_BOOT_MODE1__GPIO_4_31,
};

static int __init vmx25_setup_gpios(void)
{
	vmx25_gpio_config(vmx25_gpios, ARRAY_SIZE(vmx25_gpios));
	return 0;
}
late_initcall(vmx25_setup_gpios);

static void __init vmx25_fixup(struct machine_desc *desc, struct tag *tags,
				   char **cmdline, struct meminfo *mi)
{
}

static void __init vmx25_timer_init(void)
{
	mx25_clocks_init();
}

static struct sys_timer vmx25_timer = {
	.init	= vmx25_timer_init,
};

static int __init vmx_mod_type_setup(char *line)
{
	get_option(&line, &vmx_mod_type);
	printk(KERN_INFO "%s: Module type set to 0x%02x by kernel cmd line\n",
		__FUNCTION__, vmx_mod_type);
	return 1;
}
__setup("module_type=", vmx_mod_type_setup);

MACHINE_START(VMX25, "Voipac VMX25 module (Freescale i.MX25)")
	/* Maintainer: <support@voipac.com> */
	.phys_io	= MX25_AIPS1_BASE_ADDR,
	.io_pg_offst	= ((MX25_AIPS1_BASE_ADDR_VIRT) >> 18) & 0xfffc,
	.fixup		= vmx25_fixup,
	.boot_params    = MX25_PHYS_OFFSET + 0x100,
	.map_io		= mx25_map_io,
	.init_irq	= mx25_init_irq,
	.init_machine	= vmx25_board_init,
	.timer		= &vmx25_timer,
MACHINE_END

