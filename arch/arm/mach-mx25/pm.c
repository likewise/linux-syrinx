/*
 *  Copyright 2009 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <linux/kernel.h>
#include <linux/suspend.h>
#include <linux/io.h>
#include <mach/hardware.h>

#include <linux/pm.h>
#include <linux/sched.h>
#include <linux/proc_fs.h>

#include <mach/system.h>

#include <linux/irq.h>
#include <linux/interrupt.h>

#define CRM_BASE        MX25_IO_ADDRESS(MX25_CRM_BASE_ADDR)

#define CCM_MPCTL       0x00
#define CCM_UPCTL       0x04
#define CCM_CCTL        0x08
#define CCM_CGCR0       0x0C
#define CCM_CGCR1       0x10
#define CCM_CGCR2       0x14
#define CCM_PCDR0       0x18
#define CCM_PCDR1       0x1C
#define CCM_PCDR2       0x20
#define CCM_PCDR3       0x24
#define CCM_RCSR        0x28
#define CCM_CRDR        0x2C
#define CCM_DCVR0       0x30
#define CCM_DCVR1       0x34
#define CCM_DCVR2       0x38
#define CCM_DCVR3       0x3c
#define CCM_LTR0        0x40
#define CCM_LTR1        0x44
#define CCM_LTR2        0x48
#define CCM_LTR3        0x4c
#define CCM_PMCR0	0x58
#define CCM_PMCR1	0x5c
#define CCM_PMCR2	0x60
#define CCM_MCR         0x64
#define CCM_LPIMR0	0x68
#define CCM_LPIMR1	0x6c


#define MXC_CCM_CGCR0_PER_CSI_OFFSET     0
#define MXC_CCM_CGCR0_PER_EPIT_OFFSET    1
#define MXC_CCM_CGCR0_PER_ESAI_OFFSET    2
#define MXC_CCM_CGCR0_PER_ESDHC1_OFFSET  3
#define MXC_CCM_CGCR0_PER_ESDHC2_OFFSET  4
#define MXC_CCM_CGCR0_PER_GPT_OFFSET     5
#define MXC_CCM_CGCR0_PER_I2C_OFFSET     6
#define MXC_CCM_CGCR0_PER_LCDC_OFFSET    7
#define MXC_CCM_CGCR0_PER_NFC_OFFSET     8
#define MXC_CCM_CGCR0_PER_OWIRE_OFFSET   9
#define MXC_CCM_CGCR0_PER_PWM_OFFSET     10
#define MXC_CCM_CGCR0_PER_SIM1_OFFSET    11
#define MXC_CCM_CGCR0_PER_SIM2_OFFSET    12
#define MXC_CCM_CGCR0_PER_SSI1_OFFSET    13
#define MXC_CCM_CGCR0_PER_SSI2_OFFSET    14
#define MXC_CCM_CGCR0_PER_UART_OFFSET    15
#define MXC_CCM_CGCR0_HCLK_ATA_OFFSET    16
#define MXC_CCM_CGCR0_HCLK_BROM_OFFSET   17
#define MXC_CCM_CGCR0_HCLK_CSI_OFFSET    18
#define MXC_CCM_CGCR0_HCLK_EMI_OFFSET    19
#define MXC_CCM_CGCR0_HCLK_ESAI_OFFSET   20
#define MXC_CCM_CGCR0_HCLK_ESDHC1_OFFSET 21
#define MXC_CCM_CGCR0_HCLK_ESDHC2_OFFSET 22
#define MXC_CCM_CGCR0_HCLK_FEC_OFFSET    23
#define MXC_CCM_CGCR0_HCLK_LCDC_OFFSET   24
#define MXC_CCM_CGCR0_HCLK_RTIC_OFFSET   25
#define MXC_CCM_CGCR0_HCLK_SDMA_OFFSET   26
#define MXC_CCM_CGCR0_HCLK_SLCDC_OFFSET  27
#define MXC_CCM_CGCR0_HCLK_USBOTG_OFFSET 28

#define MXC_CCM_CGCR1_AUDMUX_OFFSET      0
#define MXC_CCM_CGCR1_ATA_OFFSET         1
#define MXC_CCM_CGCR1_CAN1_OFFSET        2
#define MXC_CCM_CGCR1_CAN2_OFFSET        3
#define MXC_CCM_CGCR1_CSI_OFFSET         4
#define MXC_CCM_CGCR1_CSPI1_OFFSET       5
#define MXC_CCM_CGCR1_CSPI2_OFFSET       6
#define MXC_CCM_CGCR1_CSPI3_OFFSET       7
#define MXC_CCM_CGCR1_DRYICE_OFFSET      8
#define MXC_CCM_CGCR1_ECT_OFFSET         9
#define MXC_CCM_CGCR1_EPIT1_OFFSET       10
#define MXC_CCM_CGCR1_EPIT2_OFFSET       11
#define MXC_CCM_CGCR1_ESAI_OFFSET        12
#define MXC_CCM_CGCR1_ESDHC1_OFFSET      13
#define MXC_CCM_CGCR1_ESDHC2_OFFSET      14
#define MXC_CCM_CGCR1_FEC_OFFSET         15
#define MXC_CCM_CGCR1_GPIO1_OFFSET       16
#define MXC_CCM_CGCR1_GPIO2_OFFSET       17
#define MXC_CCM_CGCR1_GPIO3_OFFSET       18
#define MXC_CCM_CGCR1_GPT1_OFFSET        19
#define MXC_CCM_CGCR1_GPT2_OFFSET        20
#define MXC_CCM_CGCR1_GPT3_OFFSET        21
#define MXC_CCM_CGCR1_GPT4_OFFSET        22
#define MXC_CCM_CGCR1_I2C1_OFFSET        23
#define MXC_CCM_CGCR1_I2C2_OFFSET        24
#define MXC_CCM_CGCR1_I2C3_OFFSET        25
#define MXC_CCM_CGCR1_IIM_OFFSET         26
#define MXC_CCM_CGCR1_IOMUXC_OFFSET      27
#define MXC_CCM_CGCR1_KPP_OFFSET         28
#define MXC_CCM_CGCR1_LCDC_OFFSET        29
#define MXC_CCM_CGCR1_OWIRE_OFFSET       30
#define MXC_CCM_CGCR1_PWM1_OFFSET        31

#define MXC_CCM_CGCR2_PWM2_OFFSET        (32-32)
#define MXC_CCM_CGCR2_PWM3_OFFSET        (33-32)
#define MXC_CCM_CGCR2_PWM4_OFFSET        (34-32)
#define MXC_CCM_CGCR2_RNGB_OFFSET        (35-32)
#define MXC_CCM_CGCR2_RTIC_OFFSET        (36-32)
#define MXC_CCM_CGCR2_SCC_OFFSET         (37-32)
#define MXC_CCM_CGCR2_SDMA_OFFSET        (38-32)
#define MXC_CCM_CGCR2_SIM1_OFFSET        (39-32)
#define MXC_CCM_CGCR2_SIM2_OFFSET        (40-32)
#define MXC_CCM_CGCR2_SLCDC_OFFSET       (41-32)
#define MXC_CCM_CGCR2_SPBA_OFFSET        (42-32)
#define MXC_CCM_CGCR2_SSI1_OFFSET        (43-32)
#define MXC_CCM_CGCR2_SSI2_OFFSET        (44-32)
#define MXC_CCM_CGCR2_TCHSCRN_OFFSET     (45-32)
#define MXC_CCM_CGCR2_UART1_OFFSET       (46-32)
#define MXC_CCM_CGCR2_UART2_OFFSET       (47-32)
#define MXC_CCM_CGCR2_UART3_OFFSET       (48-32)
#define MXC_CCM_CGCR2_UART4_OFFSET       (49-32)
#define MXC_CCM_CGCR2_UART5_OFFSET       (50-32)
#define MXC_CCM_CGCR2_WDOG_OFFSET        (51-32)

#define CCM_CGCR0_STOP_MODE_MASK	\
			((1 << MXC_CCM_CGCR0_HCLK_SLCDC_OFFSET) | \
			(1 << MXC_CCM_CGCR0_HCLK_RTIC_OFFSET) | \
			(1 << MXC_CCM_CGCR0_HCLK_EMI_OFFSET) | \
			(1 << MXC_CCM_CGCR0_HCLK_BROM_OFFSET))

#define CCM_CGCR1_STOP_MODE_MASK	\
			((1 << MXC_CCM_CGCR1_IIM_OFFSET) | \
			(1 << MXC_CCM_CGCR1_CAN2_OFFSET) | \
			(1 << MXC_CCM_CGCR1_CAN1_OFFSET))

#define CCM_CGCR2_STOP_MODE_MASK	\
			((1 << MXC_CCM_CGCR2_SPBA_OFFSET) | \
			(1 << MXC_CCM_CGCR2_SDMA_OFFSET) | \
			(1 << MXC_CCM_CGCR2_RTIC_OFFSET))

#define CCM_LPIMR0_MASK             0xFFFFFFFF
#define CCM_LPIMR1_MASK             0xFFFFFFFF

#define MXC_CCM_PMCR2_VSTBY             (1 << 17)
#define MXC_CCM_PMCR2_OSC24M_DOWN       (1 << 16)

#define CCM_CCTL_LP_CTL_OFFSET  24
#define CCM_CCTL_LP_CTL_MASK    (0x3 << 24)
#define CCM_CCTL_LP_MODE_RUN    (0x0 << 24)

enum mxc_cpu_pwr_mode {
        WAIT_CLOCKED,           /* wfi only */
        WAIT_UNCLOCKED,         /* WAIT */
        WAIT_UNCLOCKED_POWER_OFF,       /* WAIT + SRPG */
        STOP_POWER_ON,          /* just STOP */
        STOP_POWER_OFF,         /* STOP + SRPG */
};

enum mx25_low_pwr_mode {
        MX25_RUN_MODE,
        MX25_WAIT_MODE,
        MX25_DOZE_MODE, 
        MX25_STOP_MODE
};

#define MXC_CCM_PMCR1_CPEN_EMI          (1 << 29)
#define MXC_CCM_PMCR1_CSPAEM_P_OFFSET   26
#define MXC_CCM_PMCR1_CSPAEM_N_OFFSET   24
#define MXC_CCM_PMCR1_CSPAEM_MASK       (0xf << 24)
#define MXC_CCM_PMCR1_WBCN_OFFSET       16
#define MXC_CCM_PMCR1_CPEN              (1 << 13)
#define MXC_CCM_PMCR1_CSPA_P_OFFSET     11
#define MXC_CCM_PMCR1_CSPA_N_OFFSET     9
#define MXC_CCM_PMCR1_CSPA_MASK         (0xf << 9)

#define MXC_CCM_PMCR1_WBCN_MASK         (0xff << 16)
#define MXC_CCM_PMCR1_WBCN_DEFAULT      0xa0
#define MXC_CCM_PMCR1_WBB_INCR          0
#define MXC_CCM_PMCR1_WBB_MODE          1
#define MXC_CCM_PMCR1_WBB_DECR          2
#define MXC_CCM_PMCR1_WBB_MINI          3

#define MXC_CCM_PMCR1_AWB_EN		(MXC_CCM_PMCR1_CPEN_EMI | \
					MXC_CCM_PMCR1_CPEN | \
					(MXC_CCM_PMCR1_WBCN_DEFAULT << \
					MXC_CCM_PMCR1_WBCN_OFFSET))

#define MXC_CCM_PMCR1_WBB_DEFAULT	((MXC_CCM_PMCR1_WBB_MINI << \
					MXC_CCM_PMCR1_CSPAEM_P_OFFSET) | \
					(MXC_CCM_PMCR1_WBB_MINI << \
					MXC_CCM_PMCR1_CSPAEM_N_OFFSET) | \
					(MXC_CCM_PMCR1_WBB_MINI << \
					MXC_CCM_PMCR1_CSPA_P_OFFSET) | \
					(MXC_CCM_PMCR1_WBB_MINI << \
					MXC_CCM_PMCR1_CSPA_N_OFFSET))


#define MXC_CCM_PMCR1_AWB_DEFAULT	(MXC_CCM_PMCR1_AWB_EN | \
					MXC_CCM_PMCR1_WBB_DEFAULT)

#define MXC_CCM_PMCR1_WBCN_MASK         (0xff << 16)
#define MXC_CCM_PMCR1_CSPAEM_MASK       (0xf << 24)
#define MXC_CCM_PMCR1_CSPA_MASK         (0xf << 9)

/*!
 * This function is used to set cpu low power mode before WFI instruction
 *
 * @param  mode         indicates different kinds of power modes
 */
void mxc_cpu_lp_set(enum mxc_cpu_pwr_mode mode)
{
        unsigned int lpm;
        unsigned long reg;
        unsigned int pmcr1, pmcr2, lpimr;
        unsigned int l_cgcr0, l_cgcr1, l_cgcr2;
        struct irq_desc *desc;
        int i;

        /*read CCTL value */
        reg = __raw_readl(CRM_BASE + CCM_CCTL);

        switch (mode) {
        case WAIT_UNCLOCKED_POWER_OFF:
                lpm = MX25_DOZE_MODE;
                break;

        case STOP_POWER_ON:
        case STOP_POWER_OFF:
		lpm = MX25_STOP_MODE;
		/* The clock of LCDC/SLCDC, SDMA, RTIC, RNGC, MAX, CAN
		   and EMI needs to be gated on when entering Stop mode.
		 */
		l_cgcr0 = __raw_readl(CRM_BASE + CCM_CGCR0);
		l_cgcr1 = __raw_readl(CRM_BASE + CCM_CGCR1);
		l_cgcr2 = __raw_readl(CRM_BASE + CCM_CGCR2);
		__raw_writel(l_cgcr0 | CCM_CGCR0_STOP_MODE_MASK,
			CRM_BASE + CCM_CGCR0);
		__raw_writel(l_cgcr1 | CCM_CGCR1_STOP_MODE_MASK,
			CRM_BASE + CCM_CGCR1);
		__raw_writel(l_cgcr2 | CCM_CGCR2_STOP_MODE_MASK,
			CRM_BASE + CCM_CGCR2);
		/* The interrupts which are not wake-up sources need
		  be mask when entering Stop mode.
		 */
		lpimr = CCM_LPIMR0_MASK;
		for (i = 0; i < 32; i++) {
			desc = irq_desc + i;
			if ((desc->status & IRQ_WAKEUP) != 0)
				lpimr &= ~(1 << i);
		}
		__raw_writel(lpimr, CRM_BASE + CCM_LPIMR0);
		lpimr = CCM_LPIMR1_MASK;
		for (i = 32; i < 64; i++) {
			desc = irq_desc + i;
			if ((desc->status & IRQ_WAKEUP) != 0)
				lpimr &= ~(1 << (i - 32));
		}
		__raw_writel(lpimr, CRM_BASE + CCM_LPIMR1);

		if (mode == STOP_POWER_OFF) {
			pmcr2 = __raw_readl(CRM_BASE + CCM_PMCR2);
			pmcr2 |= (MXC_CCM_PMCR2_OSC24M_DOWN);
			__raw_writel(pmcr2, CRM_BASE + CCM_PMCR2);
			pmcr1 = __raw_readl(CRM_BASE + CCM_PMCR1);
			pmcr1 &= ~(MXC_CCM_PMCR1_WBCN_MASK |
				MXC_CCM_PMCR1_CSPAEM_MASK |
				MXC_CCM_PMCR1_CSPA_MASK);
			pmcr1 |= MXC_CCM_PMCR1_AWB_DEFAULT;
			__raw_writel(pmcr1, CRM_BASE + CCM_PMCR1);
		}
		break;

	case WAIT_CLOCKED:
	case WAIT_UNCLOCKED:
	default:
		/* Wait is the default mode used when idle. */
		lpm = MX25_WAIT_MODE;
		break;
	}

        /* program LP CTL bit */
        reg = ((reg & (~CCM_CCTL_LP_CTL_MASK)) |
               lpm << CCM_CCTL_LP_CTL_OFFSET);

        __raw_writel(reg, CRM_BASE + CCM_CCTL);
}

/*!
 * @defgroup MSL_MX25 i.MX25 Machine Specific Layer (MSL)
 */

/*!
 * @file mach-mx25/pm.c
 * @brief This file contains suspend operations
 *
 * @ingroup MSL_MX25
 */
static unsigned int cgcr0, cgcr1, cgcr2;

static int mx25_suspend_enter(suspend_state_t state)
{
	unsigned int reg;

	pr_debug("%s: Entering state %d\n", __FUNCTION__, state);
	switch (state) {
	case PM_SUSPEND_MEM:
		mxc_cpu_lp_set(STOP_POWER_OFF);
		break;
	case PM_SUSPEND_STANDBY:
		mxc_cpu_lp_set(WAIT_UNCLOCKED_POWER_OFF);
		break;
	default:
		return -EINVAL;
	}
	/* Executing CP15 (Wait-for-Interrupt) Instruction */
	cpu_do_idle();

	reg = (__raw_readl(CRM_BASE + CCM_CGCR0) & ~CCM_CGCR0_STOP_MODE_MASK) |
	    cgcr0;
	__raw_writel(reg, CRM_BASE + CCM_CGCR0);

	reg = (__raw_readl(CRM_BASE + CCM_CGCR1) & ~CCM_CGCR1_STOP_MODE_MASK) |
	    cgcr1;
	__raw_writel(reg, CRM_BASE + CCM_CGCR1);

	reg = (__raw_readl(CRM_BASE + CCM_CGCR2) & ~CCM_CGCR2_STOP_MODE_MASK) |
	    cgcr2;
	__raw_writel(reg, CRM_BASE + CCM_CGCR2);

	return 0;
}

/*
 * Called after processes are frozen, but before we shut down devices.
 */
static int mx25_suspend_prepare(void)
{
	cgcr0 = __raw_readl(CRM_BASE + CCM_CGCR0) & CCM_CGCR0_STOP_MODE_MASK;
	cgcr1 = __raw_readl(CRM_BASE + CCM_CGCR1) & CCM_CGCR1_STOP_MODE_MASK;
	cgcr2 = __raw_readl(CRM_BASE + CCM_CGCR2) & CCM_CGCR2_STOP_MODE_MASK;

	return 0;
}

/*
 * Called after devices are re-setup, but before processes are thawed.
 */
static void mx25_suspend_finish(void)
{
}

static int mx25_pm_valid(suspend_state_t state)
{
	return state > PM_SUSPEND_ON && state <= PM_SUSPEND_MAX;
}

struct platform_suspend_ops mx25_suspend_ops = {
	.valid = mx25_pm_valid,
	.prepare = mx25_suspend_prepare,
	.enter = mx25_suspend_enter,
	.finish = mx25_suspend_finish,
	.enter = mx25_suspend_enter,
};

static int __init mx25_pm_init(void)
{
	pr_info("Static Power Management for Freescale i.MX25\n");
	suspend_set_ops(&mx25_suspend_ops);

	return 0;
}

late_initcall(mx25_pm_init);
