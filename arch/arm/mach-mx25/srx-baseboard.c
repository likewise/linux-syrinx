/*
 * arch/arm/mach-mx25/vmx-baseboard.c
 *
 * Copyright (C) 2010  Voipac <support@voipac.com>
 *
 * Based on arch/arm/mach-mx25/stk5-baseboard.c
 * Copyright (C) 2009  Lothar Wassmann <LW@KARO-electronics.de>
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the:
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301
 *
 * This file adds support for devices found on Voipac baseboard
 */

#include <linux/types.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/init.h>
#include <linux/ioport.h>
#include <linux/platform_device.h>
#include <linux/input.h>
#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/fb.h>
#include <linux/serial.h>
#include <linux/fsl_devices.h>
#include <linux/irq.h>
#include <linux/mmc/host.h>
#include <linux/leds.h>
#include <linux/dma-mapping.h>
#include <linux/pwm_backlight.h>
#include <linux/i2c.h>
#include <linux/spi/spi.h>
#include <linux/spi/eeprom.h>
#include <linux/spi/ads7846.h>
#include <linux/can/platform/mcp251x.h>

#include <asm/setup.h>
#include <asm/irq.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/time.h>

#include <mach/common.h>
#include <mach/hardware.h>
#include <mach/gpio.h>
#include <mach/iomux.h>
#include <mach/iomux-v1.h>
#include <mach/iomux-mx25.h>
#include <mach/irqs.h>
#include <mach/clock.h>
#include <mach/imxfb.h>
#include <mach/mmc.h>
#include <mach/mxc_ehci.h>
#include <mach/board-vmx25.h>
#include <mach/sdhci.h>
#include <mach/i2c.h>

#include <linux/input/matrix_keypad.h>

#include <mach/ssi.h>
#include <mach/audmux.h>

#include "devices.h"
#include "devices-imx25.h"

/*
 * 1.uart
 * 2.usb
 * 3.i2c
 * 5.flexcan
 * 6.spican
 * 4.spi
 * 7.keypad
 * 8.leds
 * 9.audio
 * 10.radio
 */

#define UART1_ENABLED		1
#define UART2_ENABLED		1
#define UART3_ENABLED		0 /* conflicts with SPI */
#define UART4_ENABLED		0 /* conflicts with SSI */
#define UART5_ENABLED		1

//static int sd_slot = 0;

#if defined(CONFIG_SERIAL_IMX) || defined(CONFIG_SERIAL_IMX_MODULE)
static struct pad_desc vmx_uart_pads[][4] = {
	{
		MX25_PAD_UART1_TXD__UART1_TXD,
		MX25_PAD_UART1_RXD__UART1_RXD,
		MX25_PAD_UART1_CTS__UART1_CTS,
		MX25_PAD_UART1_RTS__UART1_RTS,
	},
	{
		MX25_PAD_UART2_TXD__UART2_TXD,
		MX25_PAD_UART2_RXD__UART2_RXD,
		MX25_PAD_UART2_CTS__UART2_CTS,
		MX25_PAD_UART2_RTS__UART2_RTS,
	},
	{
		/* UART 3 not useable on VMX25 */
	},
	{
		/* UART 4 not useable on VMX25 */
	},
	{
		MX25_PAD_ECB__UART5_TXD_MUX,
		MX25_PAD_LBA__UART5_RXD_MUX,
		MX25_PAD_CS4__UART5_CTS,
		MX25_PAD_CS5__UART5_RTS,
	},
};

static int vmx_uart_init(struct platform_device *pdev)
{
	if (pdev->id >= ARRAY_SIZE(vmx_uart_pads)) {
		return -ENODEV;
	}
	mxc_iomux_v3_setup_multiple_pads(vmx_uart_pads[pdev->id],
						ARRAY_SIZE(vmx_uart_pads[pdev->id]));
	return 0;
}

static void vmx_uart_exit(struct platform_device *pdev)
{
	BUG_ON(pdev->id >= ARRAY_SIZE(vmx_uart_pads));
	mxc_iomux_v3_release_multiple_pads(vmx_uart_pads[pdev->id],
					ARRAY_SIZE(vmx_uart_pads[pdev->id]));
}

static const struct imxuart_platform_data vmx_uart_pdata[] __initconst = {
	{
		.init = vmx_uart_init,
		.exit = vmx_uart_exit,
		.flags = IMXUART_HAVE_RTSCTS,
	}, {
		.init = vmx_uart_init,
		.exit = vmx_uart_exit,
		.flags = IMXUART_HAVE_RTSCTS,
	}, {
		.init = vmx_uart_init,
		.exit = vmx_uart_exit,
		.flags = IMXUART_HAVE_RTSCTS,
	}, {
		.init = vmx_uart_init,
		.exit = vmx_uart_exit,
		.flags = IMXUART_HAVE_RTSCTS,
	}, {
		.init = vmx_uart_init,
		.exit = vmx_uart_exit,
		.flags = IMXUART_HAVE_RTSCTS,
	}, {

	},
};
#endif // CONFIG_SERIAL_IMX

#if defined(CONFIG_USB_EHCI_MXC) || defined(CONFIG_USB_EHCI_MXC_MODULE) || \
	defined(CONFIG_USB_FSL_USB2) || defined(CONFIG_USB_FSL_USB2_MODULE)
/*
 * The USB power switch (MAX893L) used on the STK5 base board
 * produces a pulse (~100us) on the OC output whenever
 * the ON input is activated. This disturbs the USB controller.
 * As a workaround don't use USB power switching.
 * If you have a hardware that works cleanly you may
 * #define USE_USB_PWR to enable port power control for
 * the EHCI controller.
 */
static struct pad_desc vmx_usbh2_pads[] = {
#ifdef USE_USB_PWR
	MX25_PAD_D9__USBH2_PWR,
	MX25_PAD_D8__USBH2_OC,
#else
	MX25_PAD_D9__GPIO_4_11,
	MX25_PAD_D8__GPIO_4_12,
#endif
};

static int vmx_usbh2_init(struct platform_device *pdev)
{
	int ret;
#ifndef USE_USB_PWR
	const int pwr_gpio = 3 * 32 + 11;
#endif

	ret = mxc_iomux_v3_setup_multiple_pads(vmx_usbh2_pads,
					ARRAY_SIZE(vmx_usbh2_pads));
#ifdef USE_USB_PWR
	if (ret) {
		return ret;
	}
#else
	ret = gpio_request(pwr_gpio, "USBH2_PWR");
	if (ret) {
		printk(KERN_INFO "%s: Failed to request GPIO %d\n", __FUNCTION__,
			pwr_gpio);
		mxc_iomux_v3_release_multiple_pads(vmx_usbh2_pads,
						ARRAY_SIZE(vmx_usbh2_pads));
		return ret;
	}

	gpio_direction_output(pwr_gpio, 1);
#endif
	if (ret != 0) {
		mxc_iomux_v3_release_multiple_pads(vmx_usbh2_pads,
						ARRAY_SIZE(vmx_usbh2_pads));
		goto exit;
	}

exit:
#ifndef USE_USB_PWR
	gpio_free(pwr_gpio);
#endif
	return ret;
}

static int vmx_usbh2_exit(struct platform_device *pdev)
{
	mxc_iomux_v3_release_multiple_pads(vmx_usbh2_pads,
					ARRAY_SIZE(vmx_usbh2_pads));
	return 0;
}
#if 0
static struct pad_desc vmx_usbotg_pads[] = {
#ifdef USE_USB_PWR
	MX25_PAD_GPIO_A__USBOTG_PWR,
	MX25_PAD_GPIO_B__USBOTG_OC,
#else
	MX25_PAD_GPIO_A__GPIO_A,
	MX25_PAD_GPIO_B__GPIO_B,
#endif
};

static int vmx_usbotg_init(struct platform_device *pdev)
{
	int ret;
#ifndef USE_USB_PWR
	const int pwr_gpio = 0 * 32 + 0;
#endif
	printk(KERN_INFO "%s: \n", __FUNCTION__);

	ret = mxc_iomux_v3_setup_multiple_pads(vmx_usbotg_pads,
					ARRAY_SIZE(vmx_usbotg_pads));
#ifdef USE_USB_PWR
	if (ret) {
		return ret;
	}
#else

	ret = gpio_request(pwr_gpio, "USBOTG_PWR");
	if (ret) {
		printk(KERN_INFO "%s: Failed to request GPIO %d\n", __FUNCTION__,
			pwr_gpio);
		mxc_iomux_v3_release_multiple_pads(vmx_usbh2_pads,
						ARRAY_SIZE(vmx_usbh2_pads));
		return ret;
	}

	gpio_direction_output(pwr_gpio, 1);
#endif
	if (ret != 0) {
		mxc_iomux_v3_release_multiple_pads(vmx_usbotg_pads,
						ARRAY_SIZE(vmx_usbotg_pads));
		goto exit;
	}

exit:
#ifndef USE_USB_PWR
	gpio_free(pwr_gpio);
#endif

	return ret;
}

static int vmx_usbotg_exit(struct platform_device *pdev)
{
	mxc_iomux_v3_release_multiple_pads(vmx_usbotg_pads,
					ARRAY_SIZE(vmx_usbotg_pads));
	return 0;
}
#endif
#endif // CONFIG_USB_EHCI_MXC || CONFIG_USB_FSL_USB2

static int otg_mode_host = 1;

static int __init vmx_otg_mode(char *options)
{
	if (!strcmp(options, "host"))
		otg_mode_host = 1;
	else if (!strcmp(options, "device"))
		otg_mode_host = 0;
	else
		pr_info("otg_mode neither \"host\" nor \"device\". "
			"Defaulting to host\n");
	return 0;
}
__setup("otg_mode=", vmx_otg_mode);

#if defined(CONFIG_USB_EHCI_MXC) || defined(CONFIG_USB_EHCI_MXC_MODULE)
static struct mxc_usbh_platform_data vmx_usbotg_pdata = {
//	.init   = vmx_usbotg_init,
//	.exit   = vmx_usbotg_exit,
	.portsc	= MXC_EHCI_MODE_UTMI | MXC_EHCI_UTMI_16BIT,
	.flags	= MXC_EHCI_INTERFACE_DIFF_UNI   /* Differential/unidirectional (6-wire) */
//		  MXC_EHCI_INTERNAL_PHY 	/* USB transceiver enable */ 
//		  MXC_EHCI_POWER_PINS_ENABLED   /* assertion of the OC input is reported to the OTG core */
};

static struct mxc_usbh_platform_data vmx_usbh2_pdata = {
	.init   = vmx_usbh2_init,
	.exit   = vmx_usbh2_exit,
	.portsc	= MXC_EHCI_MODE_SERIAL,
	.flags	= MXC_EHCI_INTERFACE_SINGLE_UNI /* Single-ended/unidirectional (6-wire) */ | \
		  MXC_EHCI_INTERNAL_PHY 	/* USB transceiver enable */ | \
		  MXC_EHCI_IPPUE_DOWN 		/* Software has control over ipp_pue_pulldwn_dpdm */
//		  MXC_EHCI_POWER_PINS_ENABLED	/* the assertion of the OC input is reported to the host core */

};
#endif // CONFIG_USB_EHCI_MXC

#if defined(CONFIG_USB_FSL_USB2) || defined(CONFIG_USB_FSL_USB2_MODULE)
static struct fsl_usb2_platform_data vmx_usb_pdata = {
	.operating_mode	= FSL_USB2_DR_DEVICE,
	.phy_mode	= FSL_USB2_PHY_UTMI,
};
#endif // CONFIG_USB_FSL_USB2

#if defined(CONFIG_I2C) || defined(CONFIG_I2C_MODULE)
#include <linux/i2c/at24.h>
#if defined(CONFIG_I2C_IMX) || defined(CONFIG_I2C_IMX_MODULE)
static struct pad_desc mxc_i2c1_pads[] = {
	MX25_PAD_I2C1_CLK__I2C1_CLK,
	MX25_PAD_I2C1_DAT__I2C1_DAT,
};
#elif defined(CONFIG_I2C_GPIO) || defined(CONFIG_I2C_GPIO_MODULE)
static struct pad_desc mxc_i2c1_pads[] = {
	MX25_PAD_I2C1_CLK__GPIO_1_12,
	MX25_PAD_I2C1_DAT__GPIO_1_13,
};
#else
#error No suitable I2C bus driver configured
#endif

static int vmx_i2c1_init(struct device *dev)
{
	return mxc_iomux_v3_setup_multiple_pads(mxc_i2c1_pads,
						ARRAY_SIZE(mxc_i2c1_pads));
}

static void vmx_i2c1_exit(struct device *dev)
{
	mxc_iomux_v3_release_multiple_pads(mxc_i2c1_pads,
					   ARRAY_SIZE(mxc_i2c1_pads));
}

static struct imxi2c_platform_data vmx_i2c1_data = {
	.init = vmx_i2c1_init,
	.exit = vmx_i2c1_exit,
	.bitrate = 100000,
};

static struct at24_platform_data srx_eeprom = {
	.byte_len = SZ_64K / 8,
	.page_size = 8192,
	.flags = AT24_FLAG_ADDR16,
};

static struct at24_platform_data vmx_eeprom = {
	.byte_len = SZ_512K / 8,
	.page_size = 128,
	.flags = AT24_FLAG_ADDR16,
};

//#define RADIO_SI4705_IRQ	(GPIO_PORTC | 16)

static struct i2c_board_info vmx_i2c1_boardinfo[] __initdata = {
//	{
//		I2C_BOARD_INFO("sgtl5000-i2c", 0x0a),
//	},
//	{
//		I2C_BOARD_INFO("radio-si4705", 0x63),
//		.irq = gpio_to_irq(RADIO_SI4705_IRQ),
//	},
	{
		I2C_BOARD_INFO("24c64", 0x50),
		.platform_data = &srx_eeprom,
	},
//	{
//		I2C_BOARD_INFO("24c512", 0x56),
//		.platform_data = &vmx_eeprom,
//	},
	{
		I2C_BOARD_INFO("m41t00", 0x68),
	},
};
#endif /* CONFIG_I2C */

#if defined(CONFIG_CAN_FLEXCAN) || defined(CONFIG_CAN_FLEXCAN_MODULE)
static struct pad_desc vmx_can1_pads[] = {
	MX25_PAD_GPIO_A__CAN1_TX,
	MX25_PAD_GPIO_B__CAN1_RX,
};
static struct pad_desc vmx_can2_pads[] = {
	MX25_PAD_GPIO_C__CAN2_TX,
	MX25_PAD_GPIO_D__CAN2_RX,
};

static int vmx_can1_init(struct device *dev)
{
	return mxc_iomux_v3_setup_multiple_pads(vmx_can1_pads,
					ARRAY_SIZE(vmx_can1_pads));
}

static int vmx_can2_init(struct device *dev)
{
	return mxc_iomux_v3_setup_multiple_pads(vmx_can2_pads,
					ARRAY_SIZE(vmx_can2_pads));
}

static void vmx_can1_exit(struct device *dev)
{
	mxc_iomux_v3_release_multiple_pads(vmx_can1_pads,
					   ARRAY_SIZE(vmx_can1_pads));
}

static void vmx_can2_exit(struct device *dev)
{
	mxc_iomux_v3_release_multiple_pads(vmx_can2_pads,
					   ARRAY_SIZE(vmx_can2_pads));
}
#endif // CONFIG_CAN_FLEXCAN
#if 0
#if defined(CONFIG_CAN_MCP251X) || defined(CONFIG_CAN_MCP251X_MODULE)
#define CAN_MCP251X_IRQ			(GPIO_PORTB |  6)
//#define CAN_MCP251X_RESET		TODO

static struct pad_desc vmx_can_mcp251x_pads[] = {
	MX25_PAD_A20__GPIO_2_6,
};

int mcp251x_setup(struct spi_device *spi)
{
	int ret;

	ret = mxc_iomux_v3_setup_multiple_pads(vmx_can_mcp251x_pads,
					ARRAY_SIZE(vmx_can_mcp251x_pads));
	if (ret) 
		return ret;

	ret = gpio_request(CAN_MCP251X_IRQ, "MCP251X CS");
	if (ret) {
		printk(KERN_INFO "%s: Failed to request GPIO %d\n", __FUNCTION__,
			CAN_MCP251X_IRQ);
		mxc_iomux_v3_release_multiple_pads(vmx_can_mcp251x_pads,
						ARRAY_SIZE(vmx_can_mcp251x_pads));
		return ret;
	}

	gpio_direction_input(CAN_MCP251X_IRQ);
	gpio_free(CAN_MCP251X_IRQ);

	return ret;
}

static struct mcp251x_platform_data mcp251x_info = {
	.oscillator_frequency = 16000000,
	.board_specific_setup = &mcp251x_setup,
	.power_enable = NULL,
	.transceiver_enable = NULL,
};
#endif // CONFIG_CAN_MCP251X

#if defined(CONFIG_TOUCHSCREEN_ADS7846) || defined(CONFIG_TOUCHSCREEN_ADS7846_MODULE)
#define TSC2046_PENDOWN (GPIO_PORTB | 7)

static struct pad_desc vmx_touch_tsc2046_pads[] = {
	MX25_PAD_A21__GPIO_2_7,
};

int tsc2046_setup(void)
{
	int ret;

	ret = mxc_iomux_v3_setup_multiple_pads(vmx_touch_tsc2046_pads,
					ARRAY_SIZE(vmx_touch_tsc2046_pads));
	if (ret) 
		return ret;

	ret = gpio_request(TSC2046_PENDOWN, "TSC2046 pendown");
	if (ret) {
		printk(KERN_INFO "%s: Failed to request GPIO %d\n", __FUNCTION__,
			TSC2046_PENDOWN);
		mxc_iomux_v3_release_multiple_pads(vmx_touch_tsc2046_pads,
						ARRAY_SIZE(vmx_touch_tsc2046_pads));
		return ret;
	}

	gpio_direction_input(TSC2046_PENDOWN);

	return ret;
}

static int ads7846_get_pendown_state(void)
{
	return !gpio_get_value(TSC2046_PENDOWN);
}

static struct ads7846_platform_data ads7846_config __initdata = {
	.model			= 7846,
	.vref_delay_usecs	= 100,
	.x_plate_ohms		= 300,
	.y_plate_ohms		= 600,
	/* debounce Filter */
	.debounce_max		= 10,		/* max number of additional readings per sample */
	.debounce_rep		= 1,		/* additional consecutive good readings required after the first two */
	.debounce_tol		= 5,		/* tolerance used for filtering */

	.get_pendown_state	 = ads7846_get_pendown_state,
	.keep_vref_on		= 1,
	.vref_mv		= 2500,
//Android
//	.x_min			= 270,
//	.y_min			= 650,
//	.x_max			= 3915,
//	.y_max			= 3660,
//	.swap_xy		= 1,
};
#endif // CONFIG_TOUCHSCREEN_ADS7846
#endif
#if defined(CONFIG_SPI_IMX) || defined(CONFIG_SPI_IMX_MODULE)

#define VMX_SPI_SS0_GPIO		(GPIO_PORTA | 16)
#define VMX_SPI_SS1_GPIO		(GPIO_PORTA | 17)
//#define VMX_SPI_FLASH_CS_GPIO		(GPIO_PORTB |  5)
//#define VMX_SPI_CAN_CS_GPIO		(GPIO_PORTB | 10)
//#define VMX_SPI_TOUCH_CS_GPIO		(GPIO_PORTB | 11)
#define SRX_GPIO_RESETFLASH           (GPIO_PORTB | 8)
static struct pad_desc vmx_cspi1_pads[] = {
	MX25_PAD_CSPI1_MOSI__CSPI1_MOSI,
	MX25_PAD_CSPI1_MISO__CSPI1_MISO,
//	MX25_PAD_CSPI1_SS0__CSPI1_SS0,
	MX25_PAD_CSPI1_SS0__GPIO_1_16,
//	MX25_PAD_CSPI1_SS1__CSPI1_SS1,
	MX25_PAD_CSPI1_SS1__GPIO_1_17,
	MX25_PAD_CSPI1_SCLK__CSPI1_SCLK,
	MX25_PAD_CSPI1_RDY__CSPI1_RDY,
//#if defined(CONFIG_CAN_MCP251X) || defined(CONFIG_CAN_MCP251X_MODULE)
//	MX25_PAD_A24__GPIO_2_10,
//#endif // CONFIG_CAN_MCP251X
//#if defined(CONFIG_TOUCHSCREEN_ADS7846) || defined(CONFIG_TOUCHSCREEN_ADS7846_MODULE)
//	MX25_PAD_A25__GPIO_2_11,
//#endif // CONFIG_TOUCHSCREEN_ADS7846
	MX25_PAD_A22__GPIO_2_8,
};

static int vmx_spi_chipselect[] = {
//	MXC_SPI_CS(0),
	VMX_SPI_SS0_GPIO,
//	MXC_SPI_CS(1),
	VMX_SPI_SS1_GPIO,
//#if defined(CONFIG_CAN_MCP251X) || defined(CONFIG_CAN_MCP251X_MODULE)
//	VMX_SPI_CAN_CS_GPIO,
//#endif // CONFIG_CAN_MCP251X
//#if defined(CONFIG_TOUCHSCREEN_ADS7846) || defined(CONFIG_TOUCHSCREEN_ADS7846_MODULE)
//	VMX_SPI_TOUCH_CS_GPIO,
//#endif // CONFIG_TOUCHSCREEN_ADS7846
};

static int vmx_cspi1_init(struct device *dev)
{
	int ret;
	ret = mxc_iomux_v3_setup_multiple_pads(vmx_cspi1_pads,
					ARRAY_SIZE(vmx_cspi1_pads));

	if (ret)
		return ret;

	ret = gpio_request(VMX_SPI_SS0_GPIO, "SPI SS0");

	if (ret == 0) {
		gpio_direction_output(VMX_SPI_SS0_GPIO, 1);
		/* Free the CS GPIO, to allow SPI driver to register it again */
		gpio_free(VMX_SPI_SS0_GPIO);
	} else {
		printk(KERN_INFO "%s: Failed to request GPIO %d\n", __FUNCTION__,
			VMX_SPI_SS0_GPIO);
	}

	ret = gpio_request(VMX_SPI_SS1_GPIO, "SPI SS1");

	if (ret == 0) {
		gpio_direction_output(VMX_SPI_SS1_GPIO, 1);
		/* Free the CS GPIO, to allow SPI driver to register it again */
		gpio_free(VMX_SPI_SS1_GPIO);
	} else {
		printk(KERN_INFO "%s: Failed to request GPIO %d\n", __FUNCTION__,
			VMX_SPI_SS1_GPIO);
	}
#if 0
#if defined(CONFIG_CAN_MCP251X) || defined(CONFIG_CAN_MCP251X_MODULE)
	ret = gpio_request(VMX_SPI_CAN_CS_GPIO, "MCP251X CS");

	if (ret == 0) {
		gpio_direction_output(VMX_SPI_CAN_CS_GPIO, 1);
		/* Free the CS GPIO, to allow SPI driver to register it again */
		gpio_free(VMX_SPI_CAN_CS_GPIO);
	} else {
		printk(KERN_INFO "%s: Failed to request GPIO %d\n", __FUNCTION__,
			VMX_SPI_CAN_CS_GPIO);
	}

#endif // CONFIG_CAN_MCP251X
#if defined(CONFIG_TOUCHSCREEN_ADS7846) || defined(CONFIG_TOUCHSCREEN_ADS7846_MODULE)
	ret = gpio_request(VMX_SPI_TOUCH_CS_GPIO, "TSC2046 CS");

	if (ret == 0) {
		gpio_direction_output(VMX_SPI_TOUCH_CS_GPIO, 1);
		/* Free the CS GPIO, to allow SPI driver to register it again */
		gpio_free(VMX_SPI_TOUCH_CS_GPIO);
	} else {
		printk(KERN_INFO "%s: Failed to request GPIO %d\n", __FUNCTION__,
			VMX_SPI_TOUCH_CS_GPIO);
	}

	tsc2046_setup();
#endif // CONFIG_TOUCHSCREEN_ADS7846
#endif
	ret = gpio_request(SRX_GPIO_RESETFLASH, "RESETFLASH");

	if (ret == 0) {
		gpio_direction_output(SRX_GPIO_RESETFLASH, 1);
		gpio_free(SRX_GPIO_RESETFLASH);
	} else {
		printk(KERN_INFO "%s: Failed to request GPIO %d\n", __FUNCTION__,
			SRX_GPIO_RESETFLASH);
	}
	return 0;
}

static void vmx_cspi1_exit(struct device *dev)
{
	mxc_iomux_v3_release_multiple_pads(vmx_cspi1_pads,
					   ARRAY_SIZE(vmx_cspi1_pads));
}

static struct spi_imx_master vmx_spi1_data = {
	.chipselect = vmx_spi_chipselect,
	.num_chipselect = ARRAY_SIZE(vmx_spi_chipselect),
};

#if defined(CONFIG_MTD_M25P80) || defined(CONFIG_MTD_M25P80_MODULE)
#include <linux/mtd/partitions.h>
#include <mtd/mtd-abi.h>
#include <linux/spi/flash.h>
#if 0
static struct mtd_partition spi_moflash_partitions[] = {
       {
                .name = "vmx_morom(spi)",
                .size = 0x00040000,
                .offset = 0,
                .mask_flags = MTD_CAP_ROM
        }, {
                .name = "vmx_morwm(spi)",
                .size = MTDPART_SIZ_FULL,
                .offset = MTDPART_OFS_APPEND,
        }
};
#endif
static struct mtd_partition spi_mbflash_partitions[] = {
       {
                .name = "vmx_mbrom(spi)",
                .size = 0x00042000,
                .offset = 0,
                .mask_flags = MTD_CAP_ROM
        }, {
                .name = "vmx_mbrwm(spi)",
                .size = MTDPART_SIZ_FULL,
                .offset = MTDPART_OFS_APPEND,
        }
};
#if 0
static struct flash_platform_data spi_moflash_data = {
        .name = "m25p80",
        .parts = spi_moflash_partitions,
        .nr_parts = ARRAY_SIZE(spi_moflash_partitions),
        .type = "sst25vf016b"
};
#endif
static struct flash_platform_data spi_mbflash_data = {
        .name = "dataflash",
        .parts = spi_mbflash_partitions,
        .nr_parts = ARRAY_SIZE(spi_mbflash_partitions),
//        .type = "sst25vf032b"
};
#endif // CONFIG_MTD_M25P80

static struct spi_board_info vmx_spi_info[] __initdata = {
#if defined(CONFIG_MTD_M25P80) || defined(CONFIG_MTD_M25P80_MODULE)
#if 0
	{
		/* the modalias must be the same as spi device driver name */
//		.modalias = "m25p80", /* Name of spi_driver for this device */
		.modalias = "mtd_dataflash", /* Name of spi_driver for this device */
		.max_speed_hz = 25000000,     /* max spi clock (SCK) speed in HZ */
		.bus_num = 0, /* Framework bus number */
		.chip_select = 0, /* On vmx it's SPI0_SS0 */
		.platform_data = &spi_moflash_data,
		.mode = SPI_MODE_3,
	},
#endif
	{
		/* the modalias must be the same as spi device driver name */
//		.modalias = "m25p80", /* Name of spi_driver for this device */
		.modalias = "mtd_dataflash", /* Name of spi_driver for this device */
		.max_speed_hz = 25000000,     /* max spi clock (SCK) speed in HZ */
		.bus_num = 0, /* Framework bus number */
		.chip_select = 1, /* On vmx it's SPI0_SS1 */
		.platform_data = &spi_mbflash_data,
		.mode = SPI_MODE_3,
	},
#else
#if defined(CONFIG_SPI_SPIDEV) || defined(CONFIG_SPI_SPIDEV_MODULE)
	{
		.modalias = "spidev",
		.max_speed_hz = 2000000,
		.bus_num = 0,
		.chip_select = 0,
	},
	{
		.modalias = "spidev",
		.max_speed_hz = 2000000,
		.bus_num = 0,
		.chip_select = 1,
	},
#endif // CONFIG_SPI_SPIDEV
#endif // CONFIG_MTD_M25P80
#if 0
#if defined(CONFIG_CAN_MCP251X) || defined(CONFIG_CAN_MCP251X_MODULE)
	{
		.modalias = "mcp2515",
		.irq = gpio_to_irq(CAN_MCP251X_IRQ),
		.max_speed_hz = 2000000,
		.bus_num = 0,
		.chip_select = 2,
		.platform_data = &mcp251x_info,
		.mode = SPI_MODE_0,
	},
#endif // CONFIG_CAN_MCP251X
#if defined(CONFIG_TOUCHSCREEN_ADS7846) || defined(CONFIG_TOUCHSCREEN_ADS7846_MODULE)
	{
		.modalias = "ads7846",
		.irq = gpio_to_irq(TSC2046_PENDOWN),
		.max_speed_hz = (125000 * 26), /* AD speed x (cmd + sample + before, after)*/
		.chip_select = 3,
		.platform_data = &ads7846_config,
		.mode = SPI_MODE_2,
	},
#endif // CONFIG_TOUCHSCREEN_ADS7846
#endif
};
#endif // CONFIG_SPI_IMX
#if 0
#if defined(CONFIG_KEYBOARD_IMX) || defined(CONFIG_KEYBOARD_IMX_MODULE)
/*
 * This array is used for mapping keypad scancodes to keyboard keycodes.
 */
static const uint32_t vmx25_kpd_keycodes[] = {
	/* specify your keymap with KEY(row, col, keycode), */
//	KEY(0, 0, KEY_POWER),
	KEY(0, 1, KEY_HOME),
	KEY(0, 3, KEY_BACK),
	KEY(2, 1, KEY_MENU),
	KEY(2, 3, KEY_ZOOM),
};

static struct matrix_keymap_data vmx25_keypad_data = {
        .keymap = vmx25_kpd_keycodes,
        .keymap_size = ARRAY_SIZE(vmx25_kpd_keycodes),
};

static struct pad_desc vmx25_keypad_pads[] __initdata = {
        MX25_PAD_KPP_ROW0__KPP_ROW0,
        MX25_PAD_KPP_ROW1__KPP_ROW1,
        MX25_PAD_KPP_ROW2__KPP_ROW2,
        MX25_PAD_KPP_ROW3__KPP_ROW3,

        MX25_PAD_KPP_COL0__KPP_COL0,
        MX25_PAD_KPP_COL1__KPP_COL1,
        MX25_PAD_KPP_COL2__KPP_COL2,
        MX25_PAD_KPP_COL3__KPP_COL3,
};

static int vmx25_keypad_init(struct device *dev)
{
        return mxc_iomux_v3_setup_multiple_pads(vmx25_keypad_pads,
                                               ARRAY_SIZE(vmx25_keypad_pads));
}

static void vmx25_keypad_exit(struct device *dev)
{
	mxc_iomux_v3_release_multiple_pads(vmx25_keypad_pads,
					   ARRAY_SIZE(vmx25_keypad_pads));
}
#endif // CONFIG_KEYBOARD_IMX
#endif
#if defined(CONFIG_LEDS_GPIO) || defined(CONFIG_LEDS_GPIO_MODULE)
#define VMX_GPIO_LED0		(GPIO_PORTB | 4)
#define VMX_GPIO_LED1		(GPIO_PORTB | 5)
#define VMX_GPIO_LED2		(GPIO_PORTB | 6)
#define VMX_GPIO_LED3		(GPIO_PORTB | 7)

static struct pad_desc vmx_led_pads[] = {
	MX25_PAD_A18__GPIO_2_4,
	MX25_PAD_A19__GPIO_2_5,
	MX25_PAD_A20__GPIO_2_6,
	MX25_PAD_A21__GPIO_2_7,
};

static struct gpio_led vmx_leds[] = {
	{
		.name = "red0",
		.default_trigger = "none",
		.gpio = VMX_GPIO_LED0,
	},
	{
		.name = "green1",
		.default_trigger = "none",
		.gpio = VMX_GPIO_LED1,
	},
	{
		.name = "green2",
		.default_trigger = "none",
		.gpio = VMX_GPIO_LED2,
	},
	{
		.name = "red3",
		.default_trigger = "heartbeat",
		.gpio = VMX_GPIO_LED3,
	},
};

static struct gpio_led_platform_data vmx_led_data = {
	.leds = vmx_leds,
	.num_leds = ARRAY_SIZE(vmx_leds),
};

static struct platform_device vmx_led_device = {
	.name = "leds-gpio",
	.id = -1,
	.dev = {
		.platform_data = &vmx_led_data,
	},
};

static int __init vmx_led_init(void)
{
	int ret;

	ret = mxc_iomux_v3_setup_multiple_pads(vmx_led_pads,
					ARRAY_SIZE(vmx_led_pads));
	if (ret)
		printk(KERN_INFO "%s: Failed to setup PADS for LEDs: %d\n",
			__FUNCTION__, ret);

	ret = gpio_request(VMX_GPIO_LED0, "LED0");
	if (ret)
		printk(KERN_INFO "%s: Failed to request GPIO%d_%d for LED: %d\n",
			__FUNCTION__, VMX_GPIO_LED0 / 32,
			VMX_GPIO_LED0 % 32, ret);

	ret = gpio_request(VMX_GPIO_LED1, "LED1");
	if (ret)
		printk(KERN_INFO "%s: Failed to request GPIO%d_%d for LED: %d\n",
			__FUNCTION__, VMX_GPIO_LED1 / 32,
			VMX_GPIO_LED1 % 32, ret);

	ret = gpio_request(VMX_GPIO_LED2, "LED2");
	if (ret)
		printk(KERN_INFO "%s: Failed to request GPIO%d_%d for LED: %d\n",
			__FUNCTION__, VMX_GPIO_LED2 / 32,
			VMX_GPIO_LED2 % 32, ret);

	ret = gpio_request(VMX_GPIO_LED3, "LED3");
	if (ret)
		printk(KERN_INFO "%s: Failed to request GPIO%d_%d for LED: %d\n",
			__FUNCTION__, VMX_GPIO_LED3 / 32,
			VMX_GPIO_LED3 % 32, ret);

	gpio_direction_output(VMX_GPIO_LED0, 0);
	gpio_direction_output(VMX_GPIO_LED1, 0);
	gpio_direction_output(VMX_GPIO_LED2, 0);
	gpio_direction_output(VMX_GPIO_LED3, 0);

	/* free the GPIO, so that the LED driver can grab it */
	gpio_free(VMX_GPIO_LED0);
	gpio_free(VMX_GPIO_LED1);
	gpio_free(VMX_GPIO_LED2);
	gpio_free(VMX_GPIO_LED3);

	return ret;
}
arch_initcall(vmx_led_init);
#endif
#if 0
#if defined(CONFIG_SND_IMX_SOC) || defined(CONFIG_SND_IMX_SOC_MODULE)
struct imx_ssi_platform_data vmx_ssi_pdata = {
	.flags = IMX_SSI_SYN | IMX_SSI_NET | IMX_SSI_USE_I2S_SLAVE,
};
#endif // CONFIG_SND_IMX_SOC

#if defined(CONFIG_SND_SOC_IMX_3STACK_SGTL5000) || defined(CONFIG_SND_SOC_IMX_3STACK_SGTL5000_MODULE)
static struct pad_desc vmx_aud4_pads[] = {
	MX25_PAD_EB0__AUD4_TXD,
	MX25_PAD_EB1__AUD4_RXD,
	MX25_PAD_RW__AUD4_TXFS,
	MX25_PAD_OE__AUD4_TXC,
};

/*!
 * This function activates DAM port 4 to enable
 * audio I/O.
 */
static int sgtl5000_plat_init(void)
{
	return mxc_iomux_v3_setup_multiple_pads(vmx_aud4_pads,
					ARRAY_SIZE(vmx_aud4_pads));
}

/*!
 * This function inactivates DAM port 4 for
 * audio I/O
 */
static int sgtl5000_plat_exit(void)
{
	mxc_iomux_v3_release_multiple_pads(vmx_aud4_pads,
					   ARRAY_SIZE(vmx_aud4_pads));
	return 0;
}

static struct mxc_audio_platform_data sgtl5000_data = {
	.ssi_num = 0,			// MASU fixme
	.src_port = 1,
	.ext_port = 4,
	.init = sgtl5000_plat_init,	/* board specific init */
	.finit = sgtl5000_plat_exit,	/* board specific finit */
	.hp_irq = 0,
	.hp_status = NULL,
	.amp_enable = NULL,
	.sysclk = 12288000,
};

static struct platform_device vmx_sgtl5000_device = {
	.name = "imx-3stack-sgtl5000",
	.dev = {
		.platform_data = &sgtl5000_data,
	},
};
#endif // CONFIG_SND_SOC_IMX_3STACK_SGTL5000

#if defined(CONFIG_RADIO_SI4705) || defined(CONFIG_RADIO_SI4705_MODULE)
static struct pad_desc vmx_radio_pads[] = {
	MX25_PAD_VSTBY_ACK__GPIO_3_18,		/* RESET_OUT_3V3 */
	MX25_PAD_UPLL_BYPCLK__GPIO_3_16,	/* SSI2_INT */
};

#define RADIO_GPIO82	(GPIO_PORTC | 18)		// Reset
#define RADIO_GPIO80	RADIO_SI4705_IRQ		// IRQ

static int radio_si4705_reset(void) {
	int err = 0;

	err |= mxc_iomux_v3_setup_multiple_pads(vmx_radio_pads,
					ARRAY_SIZE(vmx_radio_pads));

	err |= gpio_request(RADIO_GPIO82, "GPIO82");
	err |= gpio_request(RADIO_GPIO80, "GPIO80");
	if (err) {
		return err;
	}

	gpio_direction_output(RADIO_GPIO82, 0);
	gpio_direction_output(RADIO_GPIO80, 0);

	gpio_set_value(RADIO_GPIO82, 0);
	gpio_set_value(RADIO_GPIO80, 0);

	err = 0;					// FIXME active delay
	while (--err) ;

	gpio_set_value(RADIO_GPIO82, 1);
	gpio_direction_input(RADIO_GPIO80);

	gpio_free(RADIO_GPIO80);

	return 0;
}
#endif // CONFIG_RADIO_SI4705
#endif
#if defined(CONFIG_FB_IMX) || defined(CONFIG_FB_IMX_MODULE)
#define VMX_LCD_BACKLIGHT_GPIO		(GPIO_PORTA | 26)
//#define VMX_LCD_RESET_GPIO		(GPIO_PORTB | 4)
//#define VMX_LCD_POWER_GPIO		(GPIO_PORTB | 5)

/*
 * Setup GPIO for LCDC device to be active
 *
 */
static struct pad_desc mx25_lcdc_gpios[] = {
#ifdef VMX_LCD_BACKLIGHT_GPIO
//	MX25_PAD_A18__GPIO_2_4,		/* LCD Reset (active LOW) */
#if !defined(CONFIG_MXC_PWM) && !defined(CONFIG_MXC_PWM_MODULE)
	MX25_PAD_PWM__GPIO_1_26,	/* LCD Backlight brightness 0: full 1: off */
#endif
//	MX25_PAD_A19__GPIO_2_5,		/* LCD Power Enable 0: off 1: on */
#endif
	MX25_PAD_LSCLK__LSCLK,
	MX25_PAD_LD0__LD0,
	MX25_PAD_LD1__LD1,
	MX25_PAD_LD2__LD2,
	MX25_PAD_LD3__LD3,
	MX25_PAD_LD4__LD4,
	MX25_PAD_LD5__LD5,
	MX25_PAD_LD6__LD6,
	MX25_PAD_LD7__LD7,
	MX25_PAD_LD8__LD8,
	MX25_PAD_LD9__LD9,
	MX25_PAD_LD10__LD10,
	MX25_PAD_LD11__LD11,
	MX25_PAD_LD12__LD12,
	MX25_PAD_LD13__LD13,
	MX25_PAD_LD14__LD14,
	MX25_PAD_LD15__LD15,
	MX25_PAD_D15__LD16,
	MX25_PAD_D14__LD17,
	MX25_PAD_HSYNC__HSYNC,
	MX25_PAD_VSYNC__VSYNC,
	MX25_PAD_OE_ACD__OE_ACD,
};

static int vmx_gpio_lcdc_active(struct platform_device *dev)
{
	int ret;

	printk(KERN_INFO "%s: Setting up GPIO pins for LCD\n", __FUNCTION__);
	ret = mxc_iomux_v3_setup_multiple_pads(mx25_lcdc_gpios,
					ARRAY_SIZE(mx25_lcdc_gpios));
	if (ret) {
		printk(KERN_INFO "%s: Failed to setup GPIO pins for LCD: %d\n",
			__FUNCTION__, ret);
		return ret;
	}
#ifdef VMX_LCD_BACKLIGHT_GPIO
//	ret = gpio_request(VMX_LCD_POWER_GPIO, "LCD POWER");
//	if (ret) {
//		printk(KERN_INFO "%s: Failed to request GPIO for LCD POWER: %d\n",
//			__FUNCTION__, ret);
//		goto release_pins;
//	}
#if !defined(CONFIG_MXC_PWM) && !defined(CONFIG_MXC_PWM_MODULE)
	ret = gpio_request(VMX_LCD_BACKLIGHT_GPIO, "LCD Backlight");
	if (ret) {
		printk(KERN_INFO "%s: Failed to request GPIO for backlight control: %d\n",
			__FUNCTION__, ret);
		goto free_gpio1;
	}
#endif
//	ret = gpio_request(VMX_LCD_RESET_GPIO, "LCD RESET");
//	if (ret) {
//		printk(KERN_INFO "%s: Failed to request GPIO for LCD RESET: %d\n",
//			__FUNCTION__, ret);
//		goto free_gpio2;
//	}

//	gpio_direction_output(VMX_LCD_POWER_GPIO, 1);
#if !defined(CONFIG_MXC_PWM) && !defined(CONFIG_MXC_PWM_MODULE)
	gpio_direction_output(VMX_LCD_BACKLIGHT_GPIO, 1);
#endif
//	gpio_direction_output(VMX_LCD_RESET_GPIO, 0);
#endif
	return 0;

//free_gpio2:
#if !defined(CONFIG_MXC_PWM) && !defined(CONFIG_MXC_PWM_MODULE)
	gpio_free(VMX_LCD_BACKLIGHT_GPIO);
free_gpio1:
#endif
//	gpio_free(VMX_LCD_POWER_GPIO);
//release_pins:
	mxc_iomux_v3_release_multiple_pads(mx25_lcdc_gpios,
					ARRAY_SIZE(mx25_lcdc_gpios));
	return ret;
}

/*
 * Setup GPIO for LCDC device to be inactive
 *
 */
static void vmx_gpio_lcdc_inactive(struct platform_device *dev)
{
	mxc_iomux_v3_release_multiple_pads(mx25_lcdc_gpios,
					ARRAY_SIZE(mx25_lcdc_gpios));
}

#ifdef VMX_LCD_BACKLIGHT_GPIO
#if !defined(CONFIG_MXC_PWM) && !defined(CONFIG_MXC_PWM_MODULE)
static void vmx_lcdc_backlight(int on)
{
	printk(KERN_INFO "%s: Switching LCD backlight %s\n", __FUNCTION__, on ? "on" : "off");
	if (on) {
		gpio_set_value(VMX_LCD_BACKLIGHT_GPIO, 0);
	} else {
		gpio_set_value(VMX_LCD_BACKLIGHT_GPIO, 1);
	}
}
#else
#define vmx_lcdc_backlight	NULL
#endif

static void vmx_lcdc_power(int on)
{
	printk(KERN_INFO "%s: Switching LCD reset %s\n", __FUNCTION__, on ? "off" : "on");
#if defined(CONFIG_TOUCHSCREEN_IMX_ADC) && defined(CONFIG_TOUCHSCREEN_IMX_ADC_MODULE)
	if (on) {
		/* Enable HSYNC bit of touch screen */
		imx_adc_set_hsync(1);
//		gpio_set_value(VMX_LCD_RESET_GPIO, 1);
	} else {
		/* disable HSYNC bit of touchscreen */
		imx_adc_set_hsync(0);
//		gpio_set_value(VMX_LCD_RESET_GPIO, 0);
	}
#endif // CONFIG_TOUCHSCREEN_IMX_ADC
}
#else
#define vmx_lcdc_backlight	NULL
#define vmx_lcdc_power		NULL
#endif

static struct imx_fb_videomode vmx_fb_modes[] = {
	{
		.bpp	= 16,
		.mode = {
			.name = "Innolux A080-52-TT-31 16",
			.pixclock	= KHZ2PICOS(40000)/* 28571 */,

			.xres		= 800,
			.yres		= 600,

			.hsync_len	= 20,
			.left_margin	= 30, /* 210 */
			.right_margin	= 46,

			.vsync_len	= 10,
			.upper_margin	= 23,
			.lower_margin	= 12,
		},
		.pcr	= PCR_TFT | PCR_COLOR |  PCR_PBSIZ_8 |
		PCR_BPIX_16 | PCR_FLMPOL | PCR_LPPOL | PCR_SCLK_SEL | PCR_CLKPOL,
	},
	{
		/* fH=31.5KHz HSYNC=770px, fV=60Hz VSYNC=525lines, fCLK=24000*/
		/* OPTREX t-51750gd065j-lw */
		.bpp	= 16,
		.mode = {
			.name           = "VGA-LCD",
			.pixclock       = KHZ2PICOS(24000),

			.xres           = 640,
			.yres           = 480,

			.hsync_len      = 34,
			.left_margin    = 76+8,	/* Back porch */
			.right_margin   = 10,	/* Front porch */

			.vsync_len      = 5,
			.upper_margin   = 20,	/* Back porch */
			.lower_margin   = 20,	/* Front porch */

		},
		.pcr	= PCR_TFT | PCR_COLOR | PCR_PBSIZ_8 | PCR_BPIX_16 |
		PCR_LPPOL | PCR_CLKPOL | PCR_SCLK_SEL,
	},
	{
		.bpp	= 16,
		.mode = {
			.name		= "VGA-16@60",

			.pixclock	= KHZ2PICOS(24000),
			.xres		= 640,
			.yres		= 480,

			.hsync_len	= 44,
			.left_margin	= 66+8,	/* Back porch */
			.right_margin	= 2,	/* Front porch */

			.vsync_len	= 2,
			.upper_margin	= 25+8,	/* Back porch */
			.lower_margin	= 2+8,	/* Front porch */
		},

		.pcr	= PCR_TFT | PCR_COLOR | PCR_BPIX_16 |
		PCR_FLMPOL | PCR_LPPOL | PCR_SCLK_SEL,
	},
	{
		.bpp	= 32,
		.mode = {
			.name		= "VGA-32@60",

			.pixclock	= KHZ2PICOS(24000),
			.xres		= 640,
			.yres		= 480,

			.hsync_len	= 44,
			.left_margin	= 66+8,	/* Back porch */
			.right_margin	= 2,	/* Front porch */

			.vsync_len	= 2,
			.upper_margin	= 25+8,	/* Back porch */
			.lower_margin	= 2+8,	/* Front porch */
		},

		.pcr	= PCR_TFT | PCR_COLOR | PCR_BPIX_18 |
		PCR_FLMPOL | PCR_LPPOL | PCR_SCLK_SEL | PCR_END_SEL,
	},
	{
		/* fH=30KHz HSYNC=1000px, fV=60Hz VSYNC=500lines, fCLK=30000*/
		/* DATAIMAGE FG0700K5DSSWAGT1 */
		.bpp	= 16,
		.mode = {
			.name           = "WVGA-LCD",
			.pixclock       = KHZ2PICOS(30000),

			.xres           = 800,
			.yres           = 480,

			.hsync_len      = 64,
			.left_margin    = 120,	/* Back porch */
			.right_margin   = 16,	/* Front porch */

			.vsync_len      = 2,
			.upper_margin   = 16,	/* Back porch */
			.lower_margin   = 2,	/* Front porch */

		},

		.pcr	= PCR_TFT | PCR_COLOR | PCR_PBSIZ_8 | PCR_BPIX_16 |
		PCR_FLMPOL | PCR_LPPOL | PCR_CLKPOL | PCR_SCLK_SEL,
	},
	{
		.bpp	= 16,
		.mode = {
			.name           = "WVGA-16@60",
			.pixclock       = KHZ2PICOS(30000),

			.xres           = 800,
			.yres           = 480,

			.hsync_len      = 54,
			.left_margin    = 100,	/* Back porch */
			.right_margin   = 16,	/* Front porch */

			.vsync_len      = 4,
			.upper_margin   = 28,	/* Back porch */
			.lower_margin   = 4,	/* Front porch */

		},

		.pcr	= PCR_TFT | PCR_COLOR | PCR_PBSIZ_8 | PCR_BPIX_16 |
		PCR_FLMPOL | PCR_LPPOL | PCR_SCLK_SEL,
	},
	{
		.bpp	= 32,
		.mode = {
			.name           = "WVGA-32@60",
			.pixclock       = KHZ2PICOS(30000),

			.xres           = 800,
			.yres           = 480,

			.hsync_len      = 54,
			.left_margin    = 100,	/* Back porch */
			.right_margin   = 16,	/* Front porch */

			.vsync_len      = 4,
			.upper_margin   = 28,	/* Back porch */
			.lower_margin   = 4,	/* Front porch */

		},

		.pcr	= PCR_TFT | PCR_COLOR | PCR_PBSIZ_8 | PCR_BPIX_18 |
		PCR_FLMPOL | PCR_LPPOL | PCR_SCLK_SEL | PCR_END_SEL,

	},
	{
		.bpp	= 16,
		.mode = {
			.name           = "SVGA-16@60",
			.pixclock       = KHZ2PICOS(40000),

			.xres           = 800,
			.yres           = 600,

			.hsync_len      = 64,
			.left_margin    = 170,	/* Back porch */
			.right_margin   = 24,	/* Front porch */

			.vsync_len      = 5,
			.upper_margin   = 23,	/* Back porch */
			.lower_margin   = 2,	/* Front porch */

		},

		.pcr	= PCR_TFT | PCR_COLOR | PCR_PBSIZ_8 | PCR_BPIX_16 |
		PCR_FLMPOL | PCR_LPPOL | PCR_SCLK_SEL,
	},
	{
		.bpp	= 16,
		.mode = {
			.name           = "SVGA-16@70",
			.pixclock       = KHZ2PICOS(40000),

			.xres           = 800,
			.yres           = 600,

			.hsync_len      = 20,
			.left_margin    = 30,	/* Back porch */
			.right_margin   = 46,	/* Front porch */

			.vsync_len      = 10,
			.upper_margin   = 23,	/* Back porch */
			.lower_margin   = 12,	/* Front porch */

		},

		.pcr	= PCR_TFT | PCR_COLOR | PCR_PBSIZ_8 | PCR_BPIX_16 |
		PCR_FLMPOL | PCR_LPPOL | PCR_SCLK_SEL,
	},
	{
		.bpp	= 32,
		.mode = {
			.name           = "SVGA-32@60",
			.pixclock       = KHZ2PICOS(40000),

			.xres           = 800,
			.yres           = 600,

			.hsync_len      = 64,
			.left_margin    = 170,	/* Back porch */
			.right_margin   = 24,	/* Front porch */

			.vsync_len      = 5,
			.upper_margin   = 23,	/* Back porch */
			.lower_margin   = 2,	/* Front porch */

		},

		.pcr	= PCR_TFT | PCR_COLOR | PCR_PBSIZ_8 | PCR_BPIX_18 |
		PCR_FLMPOL | PCR_LPPOL | PCR_SCLK_SEL | PCR_END_SEL,

	},
};

static struct imx_fb_platform_data vmx_fb_data = {
	.init		= vmx_gpio_lcdc_active,
	.exit		= vmx_gpio_lcdc_inactive,
	.lcd_power	= vmx_lcdc_power,
	.backlight_power = vmx_lcdc_backlight,

	.mode		= vmx_fb_modes,
	.num_modes	= ARRAY_SIZE(vmx_fb_modes),

	.dmacr		= 0x00040060,

	.cmap_greyscale	= 0,
	.cmap_inverse	= 0,
	.cmap_static	= 0,

	.fixed_screen_cpu = NULL,
};
#endif

#if defined(CONFIG_BACKLIGHT_PWM) || defined(CONFIG_BACKLIGHT_PWM_MODULE)
static struct pad_desc vmx_pwm_pads[] = {
	MX25_PAD_PWM__PWM,
};

static int vmx_backlight_init(struct device *dev)
{
	int ret;
	ret = mxc_iomux_v3_setup_pad(&vmx_pwm_pads[0]);
	return ret;
}

static int vmx_backlight_notify(struct device *dev, int brightness)
{
	printk(KERN_INFO "%s: brightness=%d\n", __FUNCTION__, brightness);
	return brightness;
}

static void vmx_backlight_exit(struct device *dev)
{
	mxc_iomux_v3_release_pad(&vmx_pwm_pads[0]);
}

static struct platform_pwm_backlight_data vmx_backlight_data = {
	.pwm_id = 0,
	.max_brightness = 100,
	.dft_brightness = 50,
	.pwm_period_ns = KHZ2PICOS(20000), /* kHz -> ps is the same as Hz -> ns */
	.init = vmx_backlight_init,
	.notify = vmx_backlight_notify,
	.exit = vmx_backlight_exit,
};

static struct platform_device vmx_backlight_pwm_device = {
	.name = "pwm-backlight",
	.dev = {
		.platform_data = &vmx_backlight_data,
	},
};
#endif /* CONFIG_BACKLIGHT_PWM || CONFIG_BACKLIGHT_PWM_MODULE */
#if 0
#if defined(CONFIG_MMC_SDHCI_MXC) || defined(CONFIG_MMC_SDHCI_MXC_MODULE)
#define SDHC1_CD_GPIO	(GPIO_PORTD | 4)
#define SDHC1_SEL_SLOT	(GPIO_PORTD | 10)

/*
 * Resource definition for the SDHC1
 */
static struct resource vmx_sdhc1_resources[] = {
	{
		.start = MMC_SDHC1_BASE_ADDR,
		.end = MMC_SDHC1_BASE_ADDR + SZ_4K - 1,
		.flags = IORESOURCE_MEM,
	}, {
		.start = MXC_INT_SDHC1,
		.end = MXC_INT_SDHC1,
		.flags = IORESOURCE_IRQ,
	}, {
		.start = gpio_to_irq(SDHC1_CD_GPIO),
		.end = gpio_to_irq(SDHC1_CD_GPIO),
		.flags = IORESOURCE_IRQ,
	},
};

static inline int vmx_esdhci_get_irq(int id)
{
	int irq;

	switch (id) {
	case 0:
		irq = vmx_sdhc1_resources[2].start;
		break;
	default:
		BUG();
	}
	return irq;
}

static const char *vmx_esdhci_irqdesc[] = {
	"ESDHCI card 0 detect",
};

static struct pad_desc vmx_sdhc_pads[] = {
	MX25_PAD_SD1_CMD__SD1_CMD,
	MX25_PAD_SD1_CLK__SD1_CLK,
	MX25_PAD_SD1_DATA0__SD1_DATA0,
	MX25_PAD_SD1_DATA2__SD1_DATA2,
	MX25_PAD_SD1_DATA3__SD1_DATA3,
	/* card detect GPIO */
	MX25_PAD_BCLK__GPIO_4_4,
	/* SD CLK switch GPIO */
	MX25_PAD_D10__GPIO_4_10,
};

static int vmx_esdhci_status(struct device *dev)
{
	return !!gpio_get_value(SDHC1_CD_GPIO);
}

static int vmx_switch_sd_slot(struct device *dev, int cmd)
{
	switch (cmd) {
	case 0 : 	gpio_set_value(SDHC1_SEL_SLOT, 1);	// ON_MODULE
			sd_slot = 1;
			break;
	case 1 :	gpio_set_value(SDHC1_SEL_SLOT, 0);	// ON_BOARD
			sd_slot = 0;
			break;
	default:	;
			break;
	}
	
	return sd_slot;
}

static int vmx_esdhci_init(struct device *dev, irqreturn_t (*esdhci_detect_irq)(int, void *),
			void *data)
{
	int err;
	struct mmc_host *host = data;
	int id = to_platform_device(dev)->id;
	int irq = vmx_esdhci_get_irq(id);

	err = mxc_iomux_v3_setup_multiple_pads(vmx_sdhc_pads,
					ARRAY_SIZE(vmx_sdhc_pads));
	if (err) {
		return err;
	}

	err = gpio_request(SDHC1_SEL_SLOT, "SD card slot select"); 
//	if (err) {
//		return err;
//	}
	gpio_direction_output(SDHC1_SEL_SLOT, 0);

#if defined(CONFIG_VMX_SD_ON_MODULE)
	gpio_set_value(SDHC1_SEL_SLOT, 1);
	sd_slot = 1;
#elif defined(CONFIG_VMX_SD_ON_BOARD)
	gpio_set_value(SDHC1_SEL_SLOT, 0);
	sd_slot = 0;
#else
#error "Choice active SD slot internal/external"
#endif

	host->caps |= MMC_CAP_4_BIT_DATA;

	printk(KERN_INFO "%s: Requesting IRQ %d\n", __FUNCTION__, irq);
	err = request_irq(irq, esdhci_detect_irq,
			IRQF_TRIGGER_FALLING | IRQF_TRIGGER_RISING,
			vmx_esdhci_irqdesc[id], data);
	if (err) {
		dev_err(dev, "Error %d requesting ESDHCI card detect IRQ %d\n",
			err, irq);
		return err;
	}
	device_set_wakeup_capable(dev, 1);

	return 0;
}

static void vmx_esdhci_exit(struct device *dev, void *data)
{
	int id = to_platform_device(dev)->id;
	int irq = vmx_esdhci_get_irq(id);

	printk(KERN_INFO "%s: Freeing IRQ %d\n", __FUNCTION__, irq);
	free_irq(irq, data);
	gpio_free(SDHC1_SEL_SLOT);
	mxc_iomux_v3_release_multiple_pads(vmx_sdhc_pads,
					ARRAY_SIZE(vmx_sdhc_pads));
}

static int vmx_esdhci_suspend(struct device *dev)
{
	int id = to_platform_device(dev)->id;
	int irq = vmx_esdhci_get_irq(id);

	if (device_may_wakeup(dev)) {
		printk(KERN_INFO "%s: Enabling IRQ %d wakeup\n", __FUNCTION__, irq);
		return enable_irq_wake(irq);
	}
	return 0;
}

static int vmx_esdhci_resume(struct device *dev)
{
	int id = to_platform_device(dev)->id;
	int irq = vmx_esdhci_get_irq(id);

	if (device_may_wakeup(dev)) {
		printk(KERN_INFO "%s: Disabling IRQ %d wakeup\n", __FUNCTION__, irq);
		return disable_irq_wake(irq);
	}
	return 0;
}

static struct mxc_sdhci_platform_data vmx_sdhc1_data = {
	.ocr_avail = MMC_VDD_32_33 | MMC_VDD_33_34,
	.init = vmx_esdhci_init,
	.exit = vmx_esdhci_exit,
	.suspend = vmx_esdhci_suspend,
	.resume = vmx_esdhci_resume,
	.status = vmx_esdhci_status,
	.min_clk = 150000,
	.max_clk = 25000000,
	.detect_delay = 100,
#if defined(CONFIG_VMX_SD_ON_MODULE)
	.force_sd_detect=1,
#else
	.force_sd_detect=0,
#endif
	.force_sd_slot=vmx_switch_sd_slot,
};

static struct platform_device vmx_sdhc1_device = {
	.name = "sdhci",
	.id = 0,
	.dev = {
		.coherent_dma_mask = DMA_BIT_MASK(32),
		.platform_data = &vmx_sdhc1_data,
	},
	.num_resources = ARRAY_SIZE(vmx_sdhc1_resources),
	.resource = vmx_sdhc1_resources,
};
#endif
#endif
static struct platform_dev_list {
	struct platform_device *pdev;
	void *pdata;
} vmx_devices[] __initdata = {
#if defined(CONFIG_FB_IMX) || defined(CONFIG_FB_IMX_MODULE)
	{ .pdev = &mx25_fb_device, .pdata = &vmx_fb_data, },
#endif
#if defined(CONFIG_MXC_PWM) || defined(CONFIG_MXC_PWM_MODULE)
	{ .pdev = &mxc_pwm_device0, },
#endif
#if defined(CONFIG_BACKLIGHT_PWM) || defined(CONFIG_BACKLIGHT_PWM_MODULE)
	{ .pdev = &vmx_backlight_pwm_device, .pdata = &vmx_backlight_data, },
#endif
#if defined(CONFIG_LEDS_GPIO) || defined(CONFIG_LEDS_GPIO_MODULE)
	{ .pdev = &vmx_led_device, },
#endif
//#if defined(CONFIG_MMC_SDHCI_MXC) || defined(CONFIG_MMC_SDHCI_MXC_MODULE)
//	{ .pdev = &vmx_sdhc1_device, },
//#endif
//#if defined(CONFIG_SND_IMX_SOC) || defined(CONFIG_SND_IMX_SOC_MODULE)
//	{ .pdev = &imx_ssi_device0, .pdata = &vmx_ssi_pdata, },
//#endif
//#if defined(CONFIG_SND_SOC_IMX_3STACK_SGTL5000) || defined(CONFIG_SND_SOC_IMX_3STACK_SGTL5000_MODULE)
//	{ .pdev = &vmx_sgtl5000_device, },
//#endif
};
#define VMX_NUM_DEVICES		ARRAY_SIZE(vmx_devices)

static __init int vmx_board_init(void)
{
	int ret;
	int i;

	/* Do UART init */
#if defined(CONFIG_SERIAL_IMX) || defined(CONFIG_SERIAL_IMX_MODULE)
#if UART1_ENABLED
	imx25_add_imx_uart0(&vmx_uart_pdata[0]);
#endif
#if UART2_ENABLED
	imx25_add_imx_uart1(&vmx_uart_pdata[1]);
#endif
#if UART3_ENABLED
	imx25_add_imx_uart2(&vmx_uart_pdata[2]);
#endif
#if UART4_ENABLED
	imx25_add_imx_uart3(&vmx_uart_pdata[3]);
#endif
#if UART5_ENABLED
	imx25_add_imx_uart4(&vmx_uart_pdata[4]);
#endif
#endif // CONFIG_SERIAL_IMX


	// Do USB init
#if defined(CONFIG_USB_EHCI_MXC) || defined(CONFIG_USB_EHCI_MXC_MODULE)
	if (otg_mode_host) {		// HOST
		mxc_register_device(&mxc_otg, &vmx_usbotg_pdata);
	} else {			// OTG
  #if defined(CONFIG_USB_FSL_USB2) || defined(CONFIG_USB_FSL_USB2_MODULE)
		mxc_register_device(&otg_udc_device, &vmx_usb_pdata);
  #endif // CONFIG_USB_FSL_USB2
	}

	mxc_register_device(&mxc_usbh2, &vmx_usbh2_pdata);
#endif // CONFIG_USB_EHCI_MXC


	// Do I2C init
#if defined(CONFIG_I2C) || defined(CONFIG_I2C_MODULE)
	imx25_add_imx_i2c0(&vmx_i2c1_data);
	ret = i2c_register_board_info(0, vmx_i2c1_boardinfo,
				      ARRAY_SIZE(vmx_i2c1_boardinfo));
	if (ret)
		printk(KERN_ERR "Failed to register I2C board info: %d\n", ret);
#endif // CONFIG_I2C

	// Do SPI init
#if defined(CONFIG_SPI_IMX) || defined(CONFIG_SPI_IMX_MODULE)
	ret = vmx_cspi1_init(NULL);
	if (ret) {
		printk(KERN_ERR "Failed to configure CSPI1 pads\n");
	} else {
		imx25_add_spi_imx0(&vmx_spi1_data);
		ret = spi_register_board_info(vmx_spi_info,
				      ARRAY_SIZE(vmx_spi_info));
		if (ret) {
			printk(KERN_ERR "Failed to register SPI board info: %d\n", ret);
		} else {
//			vmx_cspi1_exit(NULL);
		}
	}
#endif // CONFIG_SPI_IMX


	// Do CAN1 init
#if defined(CONFIG_CAN_FLEXCAN) || defined(CONFIG_CAN_FLEXCAN_MODULE)
	ret = vmx_can1_init(NULL);
	if (ret) {
		printk(KERN_ERR "Failed to configure FLEXCAN1 pads\n");
//		vmx_can1_exit(NULL);
	} else {
		imx25_add_flexcan0(NULL);
	}
	ret = vmx_can2_init(NULL);
	if (ret) {
		printk(KERN_ERR "Failed to configure FLEXCAN2 pads\n");
//		vmx_can2_exit(NULL);
	} else {
		imx25_add_flexcan1(NULL);
	}
#endif // CONFIG_CAN_FLEXCAN
#if 0
	// Do KEYPAD init
#if defined(CONFIG_KEYBOARD_IMX) || defined(CONFIG_KEYBOARD_IMX_MODULE)
	ret = vmx25_keypad_init(NULL);
	if (ret) {
		printk(KERN_ERR "Failed to configure Keypad pads\n");
	} else {
		ret = mxc_register_device(&mxc_keypad_device, &vmx25_keypad_data);
      		if (ret) {
			printk(KERN_WARNING "%s: Failed to register platform_device: %s\n",
				__FUNCTION__, mxc_keypad_device.name);
			vmx25_keypad_exit(NULL);
		}
	}
#endif // CONFIG_KEYBOARD_IMX

	// Do audmux interconnection
#if defined(CONFIG_SND_IMX_SOC) || defined(CONFIG_SND_IMX_SOC_MODULE)
	/* SSI unit master I2S codec connected to SSI_AUD4*/
	mxc_audmux_v2_configure_port(0, 0, 0);	// reset the ports
	mxc_audmux_v2_configure_port(3, 0, 0);
	mxc_audmux_v2_configure_port(0,
			MXC_AUDMUX_V2_PTCR_SYN |
			MXC_AUDMUX_V2_PTCR_TFSDIR |
			MXC_AUDMUX_V2_PTCR_TFSEL(3) |
			MXC_AUDMUX_V2_PTCR_TCLKDIR |
			MXC_AUDMUX_V2_PTCR_TCSEL(3),
			MXC_AUDMUX_V2_PDCR_RXDSEL(3)
	);
	mxc_audmux_v2_configure_port(3,
			MXC_AUDMUX_V2_PTCR_SYN,
			MXC_AUDMUX_V2_PDCR_RXDSEL(0)
	);
#endif // CONFIG_SND_IMX_SOC

#if defined(CONFIG_RADIO_SI4705) || defined(CONFIG_RADIO_SI4705_MODULE)
	radio_si4705_reset();
#endif // CONFIG_RADIO_SI4705
#endif
	for (i = 0; i < VMX_NUM_DEVICES; i++) {
		if (vmx_devices[i].pdev == NULL) continue;
		printk(KERN_INFO "%s: Registering platform device[%d] @ %p dev %p: %s\n",
			__FUNCTION__, i, vmx_devices[i].pdev, &vmx_devices[i].pdev->dev,
			vmx_devices[i].pdev->name);
		if (vmx_devices[i].pdata) {
			ret = mxc_register_device(vmx_devices[i].pdev,
						vmx_devices[i].pdata);
		} else {
			ret = platform_device_register(vmx_devices[i].pdev);
		}
		if (ret) {
			printk(KERN_WARNING "%s: Failed to register platform_device[%d]: %s: %d\n",
				__FUNCTION__, i, vmx_devices[i].pdev->name, ret);
		}
	}
	printk(KERN_INFO "%s: Done\n", __FUNCTION__);

	return 0;
}
subsys_initcall(vmx_board_init);
