/*
 * driver/media/radio/radio-si4705.c
 *
 * Driver for SI4705 radio chip for linux 2.6.
 * This driver is for SI4705 chip from Silicon Labs.
 * The I2C protocol is used for communicate with chip.
 *
 * Based in radio-tea5764.c Copyright (C) 2008 Fabio Belavenuto
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * History:
 * 2010-11-10   masu <support@voipac.com>
 *              initial code
 *
 * TODO:
 *  add RDS support - partialy
 *  add power on/off detection test in fuctions ?
 */

#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/init.h>			/* Initdata			*/
#include <linux/videodev2.h>		/* kernel radio structs		*/
#include <linux/i2c.h>			/* I2C				*/
#include <media/v4l2-common.h>
#include <media/v4l2-ioctl.h>
#include <linux/version.h>      	/* for KERNEL_VERSION MACRO     */

#include <linux/types.h>
#include <linux/delay.h>
#include <linux/jiffies.h>
#include <linux/interrupt.h>

#include "radio-si4705.h"

/* Module Parameters */
/* Radio Nr */
static int radio_nr = -1;
module_param(radio_nr, int, 0444);
MODULE_PARM_DESC(radio_nr, "Video4linux device number to use");

/* RDS buffer blocks */
static unsigned int rds_buf = 80;
module_param(rds_buf, uint, 0444);
MODULE_PARM_DESC(rds_buf, "RDS buffer entries: *100*");

/* RDS maximum block errors */
static unsigned short max_rds_errors = 1;
/* 0 means   0  errors requiring correction */
/* 1 means 1-2  errors requiring correction (used by original USBRadio.exe) */
/* 2 means 3-5  errors requiring correction */
/* 3 means   6+ errors or errors in checkword, correction not possible */
module_param(max_rds_errors, ushort, 0644);
MODULE_PARM_DESC(max_rds_errors, "RDS maximum block errors: *1*");

/* I2C code related */
static unsigned i2c_timeout = 125;

struct si4705_device {
	struct i2c_client		*i2c_client;
	struct video_device		*videodev;
	struct si4705_reg		reg;
	struct si4705_property		reg_property;
	struct mutex			mutex;
	int				users;

	/* RDS receive buffer */
	wait_queue_head_t		read_queue;
	unsigned char			*buffer;
	unsigned int			buf_size;
	unsigned int			rd_index;
	unsigned int			wr_index;

	struct work_struct		radio_work;
};

/*
 * si4705_do_i2c_cmd - read register
 */
int si4705_do_i2c_cmd(struct si4705_device *radio, u8 * txdata, u8 txsize, u8 * rxdata, u8 rxsize)
{
	int status = 0;
	unsigned long timeout, read_time;
//	struct i2c_msg msgs[2] = {
//		{ radio->i2c_client->addr, 0, txsize, (u8 *) txdata },
//		{ radio->i2c_client->addr, I2C_M_RD, rxsize, (u8 *) rxdata },
//	};
	struct i2c_msg tmsgs[1] = {
		{ radio->i2c_client->addr, 0, txsize, (u8 *) txdata },
	};
	struct i2c_msg rmsgs[1] = {
		{ radio->i2c_client->addr, I2C_M_RD, rxsize, (u8 *) rxdata },
	};

	// write
	timeout = jiffies + msecs_to_jiffies(i2c_timeout);
	do {
		read_time = jiffies;
		status = i2c_transfer(radio->i2c_client->adapter, tmsgs, 1);

		// REVISIT: at HZ=100, this is sloooow
		msleep(1);
	} while (time_before(read_time, timeout) && status != 1);

	if (status != 1)
		return -EIO;

	//read
	timeout = jiffies + msecs_to_jiffies(i2c_timeout);
	do {
		read_time = jiffies;
		status = i2c_transfer(radio->i2c_client->adapter, rmsgs, 1);

		// REVISIT: at HZ=100, this is sloooow
		msleep(1);
	} while (time_before(read_time, timeout) && !(rxdata[0] & SI4705_STS_CTS));

	if (status != 1)
		return -EIO;

	return 0;
}

int si4705_get_property(struct si4705_device * radio, u16 prop_cmd, u16 * property) {
	u8 txdata[4];
	u8 rxdata[4] = {0, 0, 0, 0};
	u8 txsize = 0, rxsize = 4;
	int ret;

	txdata[txsize++] = si4705_cmd.GET_PROPERTY;
	txdata[txsize++] = 0;
	txdata[txsize++] = prop_cmd >> 8;
	txdata[txsize++] = prop_cmd;

	ret = si4705_do_i2c_cmd(radio, txdata, txsize, rxdata, rxsize);

	*property = (rxdata[2] << 8) + rxdata[3];

	return ret;
}

int si4705_set_property(struct si4705_device * radio, u16 prop_cmd, u16 property) {
	u8 txdata[6];
	u8 rxdata[1] = {0};
	u8 txsize = 0, rxsize = 1;
	int ret;

	txdata[txsize++] = si4705_cmd.SET_PROPERTY;
	txdata[txsize++] = 0;
	txdata[txsize++] = prop_cmd >> 8;
	txdata[txsize++] = prop_cmd;
	txdata[txsize++] = property >> 8;
	txdata[txsize++] = property;

	ret = si4705_do_i2c_cmd(radio, txdata, txsize, rxdata, rxsize);

	return ret;
}

int si4705_get_all_properties(struct si4705_device *radio) {
	int ret = 0;

	ret |= si4705_get_property(radio, si4705_prop_cmd.GPO_IEN, &(radio->reg_property.gpo_ien));
	ret |= si4705_get_property(radio, si4705_prop_cmd.DIGITAL_OUTPUT_FORMAT, &(radio->reg_property.digital_output_format));
	ret |= si4705_get_property(radio, si4705_prop_cmd.DIGITAL_OUTPUT_SAMPLE_RATE, &(radio->reg_property.digital_output_sample_rate));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.REFCLK_FREQ, &(radio->reg_property.refclk_freq));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.REFCLK_PRESCALE, &(radio->reg_property.refclk_prescale));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_DEEMPHASIS, &(radio->reg_property.fm_deemphasis));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_BLEND_STEREO_THRESHOLD, &(radio->reg_property.fm_blend_stereo_threshold));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_BLEND_MONO_THRESHOLD, &(radio->reg_property.fm_blend_mono_threshold));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_ANTENNA_INPUT, &(radio->reg_property.fm_antena_input));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_MAX_TUNE_ERROR, &(radio->reg_property.fm_max_tune_error));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_RSQ_INT_SOURCE, &(radio->reg_property.fm_rsq_int_source));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_RSQ_SNR_HI_THRESHOLD, &(radio->reg_property.fm_rsq_snr_hi_threshold));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_RSQ_SNR_LO_THRESHOLD, &(radio->reg_property.fm_rsq_snr_lo_threshold));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_RSQ_RSSI_HI_THRESHOLD, &(radio->reg_property.fm_rsq_rssi_hi_threshold));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_RSQ_RSSI_LO_THRESHOLD, &(radio->reg_property.fm_rsq_rssi_lo_threshold));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_RQS_BLEND_THRESHOLD, &(radio->reg_property.fm_rsq_blend_threshold));
	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_SOFT_MUTE_RATE, &(radio->reg_property.fm_soft_mute_rate));
	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_SOFT_MUTE_MAX_ATTENUATION, &(radio->reg_property.fm_soft_mute_max_attenuation));
	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_SOFT_MUTE_SNR_THRESHOLD, &(radio->reg_property.fm_soft_mute_snr_threshold));
	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_SEEK_BAND_BOTTOM, &(radio->reg_property.fm_seek_band_bottom));
	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_SEEK_BAND_TOP, &(radio->reg_property.fm_seek_band_top));
	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_SEEK_FREQ_SPACING, &(radio->reg_property.fm_seek_freq_spacing));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_SEEK_TUNE_SNR_THRESHOLD, &(radio->reg_property.fm_seek_tune_snr_threshold));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.FM_SEEK_TUNE_RSSI_TRESHOLD, &(radio->reg_property.fm_seek_tune_rssi_threshold));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.RDS_INT_SOURCE, &(radio->reg_property.rds_int_source));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.RDS_INT_FIFO_COUNT, &(radio->reg_property.rds_int_fifo_count));
//	ret |= si4705_get_property(radio, si4705_prop_cmd.RDS_CONFIG, &(radio->reg_property.rds_config));
	ret |= si4705_get_property(radio, si4705_prop_cmd.RX_VOLUME, &(radio->reg_property.rx_volume));
	ret |= si4705_get_property(radio, si4705_prop_cmd.RX_HARD_MUTE, &(radio->reg_property.rx_hard_mute));

	return ret;
}

/* V4L2 code related */
static struct v4l2_queryctrl radio_qctrl[] = {
	{
		.id            = V4L2_CID_AUDIO_MUTE,
		.name          = "Mute",
		.minimum       = 0,
		.maximum       = 1,
		.default_value = 1,
		.type          = V4L2_CTRL_TYPE_BOOLEAN,
	},{
		.id            = V4L2_CID_AUDIO_VOLUME,
		.name          = "Volume",
		.minimum       = 0,
		.maximum       = 0x3f,
		.step          = 1,
		.default_value = 0x20,
		.type          = V4L2_CTRL_TYPE_INTEGER,
        }

};

static int si4705_power_up(struct si4705_device *radio)
{
	u8 txdata[3];
	u8 rxdata[1] = {0};
	u8 txsize = 0, rxsize = 1;

	txdata[txsize++] = si4705_cmd.POWER_UP;
	txdata[txsize++] = SI4705_PU_INT_ENABLE;
	txdata[txsize++] = SI4705_PU_MODE_AN;

	return si4705_do_i2c_cmd(radio, txdata, txsize, rxdata, rxsize);
}

static int si4705_power_down(struct si4705_device *radio)
{
	u8 txdata[1];
	u8 rxdata[1] = {0};
	u8 txsize = 0, rxsize = 1;

	txdata[txsize++] = si4705_cmd.POWER_DOWN;

	return si4705_do_i2c_cmd(radio, txdata, txsize, rxdata, rxsize);
}

/* tune an frequency, freq is defined by v4l's TUNER_LOW, i.e. 1/16th kHz */
static void si4705_tune(struct si4705_device *radio, int freq)
{
	u8 txdata[5];
	u8 rxdata[1] = {0};
	u8 txsize = 0, rxsize = 1;

	radio->reg.tunned_freq = freq / 10000;

	txdata[txsize++] = si4705_cmd.FM_TUNE_FREQ;
	txdata[txsize++] = 0x00;
	txdata[txsize++] = radio->reg.tunned_freq >> 8;
	txdata[txsize++] = radio->reg.tunned_freq;
	txdata[txsize++] = 0x00;

	if(si4705_do_i2c_cmd(radio, txdata, txsize, rxdata, rxsize)) {
		printk(KERN_INFO "%s Error setting the frequency\n", __func__);
	}
}

static void si4705_rds(struct si4705_device *radio, u8 cmd, u8 * data)
{
	u8 txdata[2];
	u8 txsize = 0;

	txdata[txsize++] = si4705_cmd.FM_RDS_STATUS;
	txdata[txsize++] = cmd;

	if(si4705_do_i2c_cmd(radio, txdata, txsize, data, 13)) {
		printk(KERN_INFO "%s Error reading RDS data\n", __func__);
	}

//printk(KERN_INFO "Status  0x%02x '%c'\n", rxdata[0], rxdata[0]);
//printk(KERN_INFO "Resp 1  0x%02x '%c'\n", rxdata[1], rxdata[1]);
//printk(KERN_INFO "Resp 2  0x%02x '%c'\n", rxdata[2], rxdata[2]);
//printk(KERN_INFO "Used    0x%02x '%c'\n", rxdata[3], rxdata[3]);
//printk(KERN_INFO "BLOCKA  0x%02x '%c'\n", rxdata[4], rxdata[4]);
//printk(KERN_INFO "BLOCKA  0x%02x '%c'\n", rxdata[5], rxdata[5]);
//printk(KERN_INFO "BLOCKB  0x%02x '%c'\n", rxdata[6], rxdata[6]);
//printk(KERN_INFO "BLOCKB  0x%02x '%c'\n", rxdata[7], rxdata[7]);
//printk(KERN_INFO "BLOCKC  0x%02x '%c'\n", rxdata[8], rxdata[8]);
//printk(KERN_INFO "BLOCKC  0x%02x '%c'\n", rxdata[9], rxdata[9]);
//printk(KERN_INFO "BLOCKD  0x%02x '%c'\n", rxdata[10], rxdata[10]);
//printk(KERN_INFO "BLOCKD  0x%02x '%c'\n", rxdata[11], rxdata[11]);
//printk(KERN_INFO "Errors  0x%02x '%c'\n", rxdata[12], rxdata[12]);

}

static void si4705_status(struct si4705_device *radio, u8 * data)
{
	u8 txdata[1];
	u8 rxdata[1] = {0};
	u8 txsize = 0, rxsize = 1;

	txdata[txsize++] = si4705_cmd.GET_INT_STATUS;

	memset(rxdata, 0, sizeof(rxdata));

	if(si4705_do_i2c_cmd(radio, txdata, txsize, rxdata, rxsize)) {
		printk(KERN_INFO "%s Error reading RDS data\n", __func__);
	}
	*data = rxdata[0];
}

static void si4705_set_audout_mode(struct si4705_device *radio, int audmode)
{
//	struct si4705_regs *r = &radio->regs;
//	int tnctrl = r->tnctrl;
//
//	if (audmode == V4L2_TUNER_MODE_MONO)
//		r->tnctrl |= TEA5764_TNCTRL_MST;
//	else
//		r->tnctrl &= ~TEA5764_TNCTRL_MST;
//	if (tnctrl != r->tnctrl)
//		tea5764_i2c_write(radio);
}

static int si4705_get_audout_mode(struct si4705_device *radio)
{
//	struct tea5764_regs *r = &radio->regs;
//
//	if (r->tnctrl & TEA5764_TNCTRL_MST)
//		return V4L2_TUNER_MODE_MONO;
//	else
		return V4L2_TUNER_MODE_STEREO;
}

/* V4L2 vidioc */
static int vidioc_querycap(struct file *file, void  *priv,
					struct v4l2_capability *v)
{
	struct si4705_device *radio = video_drvdata(file);
	struct video_device *dev = radio->videodev;

	strlcpy(v->driver, dev->dev.driver->name, sizeof(v->driver));
	strlcpy(v->card, dev->name, sizeof(v->card));
	snprintf(v->bus_info, sizeof(v->bus_info),
		 "I2C:%s", dev_name(&dev->dev));
	v->version = RADIO_VERSION;
	v->capabilities = V4L2_CAP_TUNER | V4L2_CAP_RADIO | \
			V4L2_CAP_HW_FREQ_SEEK | V4L2_CAP_RDS_CAPTURE;
	return 0;
}

static int vidioc_g_tuner(struct file *file, void *priv,
				struct v4l2_tuner *v)
{
	u8 txdata[2];
	u8 rxdata[8] = {0};
	u8 txsize = 0, rxsize = 8;
	struct si4705_device *radio = video_drvdata(file);

	if (v->index > 0)
		return -EINVAL;

	memset(v, 0, sizeof(*v));
	strcpy(v->name, "FM");
	v->type = V4L2_TUNER_RADIO;

// MASU complete this
//	si4705_get_property(radio, si4705_prop_cmd.FM_SEEK_BAND_BOTTOM, &(radio->reg_property.fm_seek_band_bottom));
//	si4705_get_property(radio, si4705_prop_cmd.FM_SEEK_BAND_TOP, &(radio->reg_property.fm_seek_band_top));

// MASU FIXME Get signal quality

	txdata[txsize++] = si4705_cmd.FM_TUNE_STATUS;
	txdata[txsize++] = 0x00;

	si4705_do_i2c_cmd(radio, txdata, txsize, rxdata, rxsize);

	v->rangelow   = FREQ_MIN * FREQ_MUL;
	v->rangehigh  = FREQ_MAX * FREQ_MUL;
	v->capability = V4L2_TUNER_CAP_LOW | V4L2_TUNER_CAP_STEREO;
//	if (r->tunchk & TEA5764_TUNCHK_STEREO)
		v->rxsubchans = V4L2_TUNER_SUB_STEREO;
//	else
//		v->rxsubchans = V4L2_TUNER_SUB_MONO;
	v->audmode = si4705_get_audout_mode(radio);
	v->signal = rxdata[5] * 0xffff / 0xf;		// MASU fix me
//	v->afc = TEA5764_TUNCHK_IFCNT(r->tunchk);

	return 0;
}

static int vidioc_s_tuner(struct file *file, void *priv,
				struct v4l2_tuner *v)
{
	struct si4705_device *radio = video_drvdata(file);

	if (v->index > 0)
		return -EINVAL;

	si4705_set_audout_mode(radio, v->audmode);
	return 0;
}

static int vidioc_s_frequency(struct file *file, void *priv,
				struct v4l2_frequency *f)
{
	struct si4705_device *radio = video_drvdata(file);

	if (f->tuner != 0 || f->type != V4L2_TUNER_RADIO)
		return -EINVAL;
	if (f->frequency == 0) {
		/* We special case this as a power down control. */
		si4705_power_down(radio);
	}
	if (f->frequency < (FREQ_MIN * FREQ_MUL))
		return -EINVAL;
	if (f->frequency > (FREQ_MAX * FREQ_MUL))
		return -EINVAL;
	si4705_power_up(radio);
	si4705_tune(radio, (f->frequency * 125) / 2);

	return 0;
}

static int vidioc_g_frequency(struct file *file, void *priv,
				struct v4l2_frequency *f)
{
	struct si4705_device *radio = video_drvdata(file);

	if (f->tuner != 0)
		return -EINVAL;

	memset(f, 0, sizeof(*f));
	f->type = V4L2_TUNER_RADIO;
//	if (r->tnctrl & TEA5764_TNCTRL_PUPD0)
		f->frequency = (radio->reg.tunned_freq * 10000 * 2) / 125;
//	else
//		f->frequency = 0;

	return 0;
}

static int vidioc_queryctrl(struct file *file, void *priv,
			    struct v4l2_queryctrl *qc)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(radio_qctrl); i++) {
		if (qc->id && qc->id == radio_qctrl[i].id) {
			memcpy(qc, &(radio_qctrl[i]), sizeof(*qc));
			return 0;
		}
	}
	return -EINVAL;
}

static int vidioc_g_ctrl(struct file *file, void *priv,
			    struct v4l2_control *ctrl)
{
	struct si4705_device *radio = video_drvdata(file);

	switch (ctrl->id) {
	case V4L2_CID_AUDIO_VOLUME:
		si4705_get_property(radio, si4705_prop_cmd.RX_VOLUME, &(radio->reg_property.rx_volume));
		ctrl->value = radio->reg_property.rx_volume;
		return 0;
	case V4L2_CID_AUDIO_MUTE:
		si4705_get_property(radio, si4705_prop_cmd.RX_HARD_MUTE, &(radio->reg_property.rx_hard_mute));
		ctrl->value = (radio->reg_property.rx_hard_mute & SI4705_RHM_MUTE_ALL) ? 1 : 0;
		return 0;
	}
	return -EINVAL;
}

static int vidioc_s_ctrl(struct file *file, void *priv,
			    struct v4l2_control *ctrl)
{
	struct si4705_device *radio = video_drvdata(file);
	switch (ctrl->id) {
	case V4L2_CID_AUDIO_VOLUME:
		si4705_set_property(radio, si4705_prop_cmd.RX_VOLUME, ctrl->value);
		return 0;
	case V4L2_CID_AUDIO_MUTE:
		if (ctrl->value)
			si4705_set_property(radio, si4705_prop_cmd.RX_HARD_MUTE, SI4705_RHM_MUTE_ALL);
		else
			si4705_set_property(radio, si4705_prop_cmd.RX_HARD_MUTE, SI4705_RHM_MUTE_NONE);
		return 0;
	}
	return -EINVAL;
}

static int vidioc_g_input(struct file *filp, void *priv, unsigned int *i)
{
	*i = 0;
	return 0;
}

static int vidioc_s_input(struct file *filp, void *priv, unsigned int i)
{
	if (i != 0)
		return -EINVAL;
	return 0;
}

static int vidioc_g_audio(struct file *file, void *priv,
			   struct v4l2_audio *a)
{
	if (a->index > 1)
		return -EINVAL;

	strcpy(a->name, "Radio");
	a->capability = V4L2_AUDCAP_STEREO;
	return 0;
}

static int vidioc_s_audio(struct file *file, void *priv,
			   struct v4l2_audio *a)
{
	if (a->index != 0)
		return -EINVAL;

	return 0;
}

static int si4705_open(struct file *file)
{
	struct si4705_device *radio = video_drvdata(file);
	int ret = 0;
	unsigned short cerr = 0x0000;

	mutex_lock(&radio->mutex);
	radio->users++;

	if (radio->users == 1) {
		/* start radio */
#ifndef USE_LEGACY_ONOFF
		ret = si4705_power_up(radio);

		if (ret)
			goto done;
#endif
#ifdef DISSABLE_RDS
		switch(max_rds_errors) {
			case 0: cerr = 0x0000; break;
			case 1: 
			case 2: cerr = 0x5500; break;
			case 3:
			case 4:
			case 5: cerr = 0xaa00; break;
			default : cerr = 0xff00; break;
		}

		/* enable RDS interrupt */
		ret = si4705_set_property(radio, si4705_prop_cmd.GPO_IEN, SI4705_GI_RDSIEN);
		// GPIO2 int configuret at power up
		ret |= si4705_set_property(radio, si4705_prop_cmd.RDS_INT_SOURCE, 0x0001);	// enable fifo filled interrupts
		ret |= si4705_set_property(radio, si4705_prop_cmd.RDS_INT_FIFO_COUNT, 0x0004);
		ret |= si4705_set_property(radio, si4705_prop_cmd.RDS_CONFIG, cerr |0x0001);	// enable interrupt
		if (ret)
			goto done;
#endif
	}

done:
	mutex_unlock(&radio->mutex);
	return ret;
}

static int si4705_close(struct file *file)
{
	struct si4705_device *radio = video_drvdata(file);
	int ret = 0;

	if (!radio)
		return -ENODEV;
	mutex_lock(&radio->mutex);
	radio->users--;
	if (radio->users == 0)
		/* stop radio */
#ifndef USE_LEGACY_ONOFF
		ret = si4705_power_down(radio);
#endif
	mutex_unlock(&radio->mutex);

	return ret;
}

/*
 * si4705_fops_read - read RDS data
 */
static ssize_t si4705_fops_read(struct file *file, char __user *buf,
		size_t count, loff_t *ppos)
{
	struct si4705_device *radio = video_drvdata(file);
	int retval = 0;
	unsigned int block_count = 0;

	/* switch on rds reception */
//	if ((radio->registers[SYSCONFIG1] & SYSCONFIG1_RDS) == 0)
//		si470x_rds_on(radio);

	/* block if no new data available */
/*	while (radio->wr_index == radio->rd_index) {
		if (file->f_flags & O_NONBLOCK) {
			retval = -EWOULDBLOCK;
			goto done;
		}
		if (wait_event_interruptible(radio->read_queue,
			radio->wr_index != radio->rd_index) < 0) {
			retval = -EINTR;
			goto done;
		}
	}
*/
	/* calculate block count from byte count */
	count /= 3;

	/* copy RDS block out of internal buffer and to user buffer */
	mutex_lock(&radio->mutex);
/*	while (block_count < count) {
		if (radio->rd_index == radio->wr_index)
			break;

		// always transfer rds complete blocks
		if (copy_to_user(buf, &radio->buffer[radio->rd_index], 3))
			// retval = -EFAULT;
			break;

		// increment and wrap read pointer
		radio->rd_index += 3;
		if (radio->rd_index >= radio->buf_size)
			radio->rd_index = 0;

		// increment counters
		block_count++;
		buf += 3;
		retval += 3;
	}
*/
	mutex_unlock(&radio->mutex);

//done:
	return retval;
}

/* File system interface */
static const struct v4l2_file_operations si4705_fops = {
	.owner		= THIS_MODULE,
	.open           = si4705_open,
	.release        = si4705_close,
	.ioctl		= video_ioctl2,
	.read		= si4705_fops_read,
};

static const struct v4l2_ioctl_ops si4705_ioctl_ops = {
	.vidioc_querycap    = vidioc_querycap,
	.vidioc_g_tuner     = vidioc_g_tuner,
	.vidioc_s_tuner     = vidioc_s_tuner,
	.vidioc_g_audio     = vidioc_g_audio,
	.vidioc_s_audio     = vidioc_s_audio,
	.vidioc_g_input     = vidioc_g_input,
	.vidioc_s_input     = vidioc_s_input,
	.vidioc_g_frequency = vidioc_g_frequency,
	.vidioc_s_frequency = vidioc_s_frequency,
	.vidioc_queryctrl   = vidioc_queryctrl,
	.vidioc_g_ctrl      = vidioc_g_ctrl,
	.vidioc_s_ctrl      = vidioc_s_ctrl,
};

/* V4L2 interface */
static struct video_device si4705_radio_template = {
	.name		= "SI4705 FM-Radio",
	.fops           = &si4705_fops,
	.ioctl_ops 	= &si4705_ioctl_ops,
	.release	= video_device_release,
};


/*
 * si470x_i2c_interrupt_work - rds processing function
 */
#ifndef DISSABLE_RDS
static void si4705_i2c_interrupt_work(struct work_struct *work)
{
	struct si4705_device *radio = container_of(work,
		struct si4705_device, radio_work);
//	unsigned char regnr;
//	unsigned char blocknum;
//	unsigned short bler; // rds block errors
	unsigned char tmpbuf[13];
//	unsigned char finbuf[13];
//	int retval = 0;
	u8 status;

	// safety checks
	si4705_status(radio, &status);
	if ((status & SI4705_GIS_RDSINT) == 0)		// not a rds interrupt
		return;

	// Update RDS registers
	do {
		si4705_rds(radio, SI4705_FRS_INTACK, tmpbuf);
	} while (tmpbuf[3] != 0x00);	



//printk(KERN_INFO "Status  0x%02x '%c'\n", rxdata[0], rxdata[0]);
//printk(KERN_INFO "Resp 1  0x%02x '%c'\n", rxdata[1], rxdata[1]);
//printk(KERN_INFO "Resp 2  0x%02x '%c'\n", rxdata[2], rxdata[2]);
//printk(KERN_INFO "Used    0x%02x '%c'\n", rxdata[3], rxdata[3]);
//printk(KERN_INFO "BLOCKA  0x%02x '%c'\n", rxdata[4], rxdata[4]);
//printk(KERN_INFO "BLOCKA  0x%02x '%c'\n", rxdata[5], rxdata[5]);
//printk(KERN_INFO "BLOCKB  0x%02x '%c'\n", rxdata[6], rxdata[6]);
//printk(KERN_INFO "BLOCKB  0x%02x '%c'\n", rxdata[7], rxdata[7]);
//printk(KERN_INFO "BLOCKC  0x%02x '%c'\n", rxdata[8], rxdata[8]);
//printk(KERN_INFO "BLOCKC  0x%02x '%c'\n", rxdata[9], rxdata[9]);
//printk(KERN_INFO "BLOCKD  0x%02x '%c'\n", rxdata[10], rxdata[10]);
//printk(KERN_INFO "BLOCKD  0x%02x '%c'\n", rxdata[11], rxdata[11]);
//printk(KERN_INFO "Errors  0x%02x '%c'\n", rxdata[12], rxdata[12]);
/*		switch (blocknum) {
		default:
			bler = (radio->registers[STATUSRSSI] &
					STATUSRSSI_BLERA) >> 9;
			rds = radio->registers[RDSA];
			break;
		case 1:
			bler = (radio->registers[READCHAN] &
					READCHAN_BLERB) >> 14;
			rds = radio->registers[RDSB];
			break;
		case 2:
			bler = (radio->registers[READCHAN] &
					READCHAN_BLERC) >> 12;
			rds = radio->registers[RDSC];
			break;
		case 3:
			bler = (radio->registers[READCHAN] &
					READCHAN_BLERD) >> 10;
			rds = radio->registers[RDSD];
			break;
		};

		// Fill the V4L2 RDS buffer
		put_unaligned_le16(rds, &tmpbuf);
		tmpbuf[2] = blocknum;           // offset name
		tmpbuf[2] |= blocknum << 3;     // received offset
		if (bler > max_rds_errors)
			tmpbuf[2] |= 0x80;      // uncorrectable errors
		else if (bler > 0)
			tmpbuf[2] |= 0x40;      // corrected error(s)

		// copy RDS block to internal buffer
		memcpy(&radio->buffer[radio->wr_index], &tmpbuf, 3);
		radio->wr_index += 3;

		// wrap write pointer
		if (radio->wr_index >= radio->buf_size)
			radio->wr_index = 0;

		// check for overflow
		if (radio->wr_index == radio->rd_index) {
			// increment and wrap read pointer
			radio->rd_index += 3;
			if (radio->rd_index >= radio->buf_size)
				radio->rd_index = 0;
		}
	}

      if (radio->wr_index != radio->rd_index)
             wake_up_interruptible(&radio->read_queue);
*/
}
#else
static void si4705_i2c_interrupt_work(struct work_struct *work) {
	;
}
#endif // DISSABLE_RDS

/*
 * si470x_i2c_interrupt - interrupt handler
 */
static irqreturn_t si4705_i2c_interrupt(int irq, void *dev_id)
{
	struct si4705_device *radio = dev_id;

	if (!work_pending(&radio->radio_work))
		schedule_work(&radio->radio_work);

	return IRQ_HANDLED;
}

/* I2C probe: check if the device exists and register with v4l if it is */
static int __devinit si4705_i2c_probe(struct i2c_client *client,
					const struct i2c_device_id *id)
{
	struct si4705_device *radio;
	u8 txdata[2];
	u8 rxdata[8];
	u8 txsize = 0, rxsize = 0;
	int ret;

	memset(txdata, 0, sizeof(txdata));
	memset(rxdata, 0, sizeof(rxdata));

	/* private data allocation and initialization */
	radio = kzalloc(sizeof(struct si4705_device), GFP_KERNEL);
	if (!radio) {
		ret = -ENOMEM;
		goto err_initial;
	}

	INIT_WORK(&radio->radio_work, si4705_i2c_interrupt_work);
	radio->users = 0;
	radio->i2c_client = client;
	mutex_init(&radio->mutex);

	/* video device allocation and initialization */
	radio->videodev = video_device_alloc();
	if (!(radio->videodev)) {
		ret = -ENOMEM;
		goto err_radio;
	}
	memcpy(radio->videodev, &si4705_radio_template,
		sizeof(si4705_radio_template));
	video_set_drvdata(radio->videodev, radio);

	/* Detect the chip in power off mode */	
	txdata[txsize++] = si4705_cmd.POWER_UP;	// Library ID
	txdata[txsize++] = 0x0F;

	rxsize = 8;
	memset(rxdata, 0, sizeof(rxdata));

	if (si4705_do_i2c_cmd(radio, txdata, txsize, rxdata, rxsize)) {
		ret = -EIO;
		goto errrel;
	}

	radio->reg.part_number = rxdata[1];
	radio->reg.fw_major = rxdata[2];
	radio->reg.fw_minor = rxdata[3];
	radio->reg.revision = rxdata[6];


	dev_info(&client->dev, "ChipID=si47%02d%c Firmware %c%c\n",
			radio->reg.part_number, radio->reg.revision, \
			radio->reg.fw_major, radio->reg.fw_minor);
	if ((((radio->reg.fw_major - '0') * 10) + (radio->reg.fw_minor - '0')) \
			< RADIO_FW_VERSION) {
		dev_warn(&client->dev,
			"This driver is known to work with "
			"firmware version %hu,\n", RADIO_FW_VERSION);
		dev_warn(&client->dev,
			"but the device has older firmware version.\n");
	}

	/* Power on */
	if (si4705_power_up(radio)) {
		ret = -EIO;
		goto errrel;
	}

	/* Get all properties */
//	if (si4705_get_all_properties(radio)) {
//		ret = -EIO;
//		goto errrel;
//	}

	/* rds buffer allocation */
	radio->buf_size = rds_buf;
	radio->buffer = kmalloc(radio->buf_size, GFP_KERNEL);
	if (!radio->buffer) {
		ret = -EIO;
		goto errrel;
	}

	/* rds buffer configuration */
	radio->wr_index = 0;
	radio->rd_index = 0;
	init_waitqueue_head(&radio->read_queue);

	ret = request_irq(client->irq, si4705_i2c_interrupt,
			IRQF_TRIGGER_FALLING, DRIVER_NAME, radio);
	if (ret) {
		dev_err(&client->dev, "Failed to register interrupt\n");
		goto err_rds;
	}

	ret = video_register_device(radio->videodev, VFL_TYPE_RADIO, radio_nr);
	if (ret < 0) {
		dev_warn(&client->dev, "Could not register video device!");
		goto err_all;
	}

	i2c_set_clientdata(client, radio);

	return 0;
err_all:
	free_irq(client->irq, radio);
err_rds:
	kfree(radio->buffer);
errrel:
	video_device_release(radio->videodev);
err_radio:
	kfree(radio);
err_initial:
	return ret;
}

static int __devexit si4705_i2c_remove(struct i2c_client *client)
{
	struct si4705_device *radio = i2c_get_clientdata(client);

	free_irq(client->irq, radio);
	cancel_work_sync(&radio->radio_work);
#ifdef USE_LEGACY_ONOFF
	si4705_power_down(radio);
#endif
	kfree(radio->buffer);
	video_unregister_device(radio->videodev);
	kfree(radio);

	return 0;
}

/* I2C subsystem interface */
static const struct i2c_device_id si4705_i2c_id[] = {
	/* Generic Entry */
	{ "radio-si4705", 0 },
	/* Terminating entry */
	{ }
};
MODULE_DEVICE_TABLE(i2c, si4705_i2c_id);

#ifdef CONFIG_PM
/*
 * si470x_i2c_suspend - suspend the device
 */
static int si4705_i2c_suspend(struct i2c_client *client, pm_message_t mesg)
{
	struct si4705_device *radio = i2c_get_clientdata(client);

	if (si4705_power_down(radio))
		return -EIO;

	return 0;
}


/*
 * si470x_i2c_resume - resume the device
 */
static int si4705_i2c_resume(struct i2c_client *client)
{
	struct si4705_device *radio = i2c_get_clientdata(client);

	if (si4705_power_up(radio))
		return -EIO;

	return 0;
}
#else
#define si4705_i2c_suspend	NULL
#define si4705_i2c_resume	NULL
#endif

static struct i2c_driver si4705_i2c_driver = {
	.driver = {
		.name = DRIVER_NAME,
		.owner = THIS_MODULE,
	},
	.probe = si4705_i2c_probe,
	.remove = __devexit_p(si4705_i2c_remove),
	.suspend = si4705_i2c_suspend,
	.resume	 = si4705_i2c_resume,
	.id_table = si4705_i2c_id,
};

/* init the driver */
static int __init si4705_init(void)
{
	printk(KERN_INFO KBUILD_MODNAME ": " DRIVER_VERSION ": "
		DRIVER_DESC "\n");
	return i2c_add_driver(&si4705_i2c_driver);
}

/* cleanup the driver */
static void __exit si4705_exit(void)
{
	i2c_del_driver(&si4705_i2c_driver);
}

module_init(si4705_init);
module_exit(si4705_exit);

MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_VERSION(DRIVER_VERSION);
MODULE_LICENSE("GPL");
