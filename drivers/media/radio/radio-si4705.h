/*
 * drivers/media/radio/radio-si4705.h
 *
 * Property and commands definitions for Si4705 radio receiver chip.
 *
 * Copyright (c) 2010 Voipac
 *
 * This file is licensed under the terms of the GNU General Public License
 * version 2. This program is licensed "as is" without any warranty of any
 * kind, whether express or implied.
 *
 */

#ifndef RADIO_SI4705_H
#define RADIO_SI4705_H

#include <linux/types.h>


/* driver definitions */
#define DRIVER_NAME "radio-si4705"

#define DRIVER_VERSION	"v0.01"
#define RADIO_VERSION	KERNEL_VERSION(0, 0, 1)

#define DRIVER_AUTHOR	"Voipac <support@voipac.com>"
#define DRIVER_DESC	"A driver for the SI4705 radio chip."

/* Tunner will remain active until the driver is uloaded */
#define USE_LEGACY_ONOFF
#define DISSABLE_RDS

/* Tunner will remain active only when fd is open */
//#undef USE_LEGACY_ONOFF

/* Frequency limits in MHz -- these are European values.  For Japanese
devices, that would be 76000 and 91000.  */
#define FREQ_MIN  87500
#define FREQ_MAX 108000
#define FREQ_MUL 16

/* Firmware Versions */
#define RADIO_FW_VERSION        20


/* Status response mask */
#define SI4705_STS_CTS			0x80
#define SI4705_STS_ERR			0x40

/* Power Up mask */
#define SI4705_PU_INT_ENABLE		0xC0
#define SI4705_PU_MODE_AN		0x05
#define SI4705_PU_MODE_DI		0xB0
#define SI4705_PU_MODE_ANDI		(SI4705_PU_MODE_AN | SI4705_PU_MODE_DI)

/* Get int status mask */
#define SI4705_GIS_RDSINT 		0x04

/* Fm rds starus mask */
#define SI4705_FRS_MTFIFO		0x02
#define SI4705_FRS_INTACK		0x01

struct si4705_reg {
	u8 part_number;		// hex - last two digits
	u8 fw_major;		// ASCII
	u8 fw_minor;		// ASCII
	u8 revision; 		// ASCII
	u16 tunned_freq;	// hex 101.20MHz ~ 0x2788
}  __attribute__ ((packed));

typedef struct {
	u8 POWER_UP;
	u8 GET_REV;
	u8 POWER_DOWN;
	u8 SET_PROPERTY;
	u8 GET_PROPERTY;
	u8 GET_INT_STATUS;
	u8 PATCH_ARGS;
	u8 PATCH_DATA;
	u8 FM_TUNE_FREQ;
	u8 FM_SEEK_START;
	u8 FM_TUNE_STATUS;
	u8 FM_RSQ_STATUS;
	u8 FM_RDS_STATUS;
	u8 FM_AGC_STATUS;
	u8 FM_AGC_OVERRIDE;
	u8 GPIO_CTL;
	u8 GPIO_SET;
} SI4705_CMD;

SI4705_CMD si4705_cmd = {
	.POWER_UP = 0x01,
	.GET_REV = 0x10,
	.POWER_DOWN = 0x11,
	.SET_PROPERTY = 0x12,
	.GET_PROPERTY = 0x13,
	.GET_INT_STATUS = 0x14,
	.PATCH_ARGS = 0x15,
	.PATCH_DATA = 0x16,
	.FM_TUNE_FREQ = 0x20,
	.FM_SEEK_START = 0x21,
	.FM_TUNE_STATUS = 0x22,
	.FM_RSQ_STATUS = 0x23,
	.FM_RDS_STATUS = 0x24,
	.FM_AGC_STATUS = 0x27,
	.FM_AGC_OVERRIDE = 0x28,
	.GPIO_CTL = 0x80,
	.GPIO_SET = 0x81
};

/* gpo_ien */
#define SI4705_GI_RDSREP	0x0400
#define SI4705_GI_RDSIEN	0x0004

/* rx_hard_mute */
#define SI4705_RHM_MUTE_R	0x0001
#define SI4705_RHM_MUTE_L	0x0002
#define SI4705_RHM_MUTE_ALL	(SI4705_RHM_MUTE_R | SI4705_RHM_MUTE_L)
#define SI4705_RHM_MUTE_NONE	0x0000

struct si4705_property {
	u16 gpo_ien;			/* Enables interrupt sources */
	u16 digital_output_format;	/* Configure digital audio outputs */
	u16 digital_output_sample_rate;
	u16 refclk_freq;
	u16 refclk_prescale;
	u16 fm_deemphasis;
	u16 fm_blend_stereo_threshold;
	u16 fm_blend_mono_threshold;
	u16 fm_antena_input;
	u16 fm_max_tune_error;
	u16 fm_rsq_int_source;
	u16 fm_rsq_snr_hi_threshold;
	u16 fm_rsq_snr_lo_threshold;
	u16 fm_rsq_rssi_hi_threshold;
	u16 fm_rsq_rssi_lo_threshold;
	u16 fm_rsq_blend_threshold;
	u16 fm_soft_mute_rate;
	u16 fm_soft_mute_max_attenuation;
	u16 fm_soft_mute_snr_threshold;
	u16 fm_seek_band_bottom;
	u16 fm_seek_band_top;
	u16 fm_seek_freq_spacing;
	u16 fm_seek_tune_snr_threshold;
	u16 fm_seek_tune_rssi_threshold;
	u16 rds_int_source;
	u16 rds_int_fifo_count;
	u16 rds_config;
	u16 rx_volume;
	u16 rx_hard_mute;
} __attribute__ ((packed));

typedef struct {
	u16 GPO_IEN;
	u16 DIGITAL_OUTPUT_FORMAT;
	u16 DIGITAL_OUTPUT_SAMPLE_RATE;
	u16 REFCLK_FREQ;
	u16 REFCLK_PRESCALE;
	u16 FM_DEEMPHASIS;
	u16 FM_BLEND_STEREO_THRESHOLD;
	u16 FM_BLEND_MONO_THRESHOLD;
	u16 FM_ANTENNA_INPUT;
	u16 FM_MAX_TUNE_ERROR;
	u16 FM_RSQ_INT_SOURCE;
	u16 FM_RSQ_SNR_HI_THRESHOLD;
	u16 FM_RSQ_SNR_LO_THRESHOLD;
	u16 FM_RSQ_RSSI_HI_THRESHOLD;
	u16 FM_RSQ_RSSI_LO_THRESHOLD;
	u16 FM_RQS_BLEND_THRESHOLD;
	u16 FM_SOFT_MUTE_RATE;
	u16 FM_SOFT_MUTE_MAX_ATTENUATION;
	u16 FM_SOFT_MUTE_SNR_THRESHOLD;
	u16 FM_SEEK_BAND_BOTTOM;
	u16 FM_SEEK_BAND_TOP;
	u16 FM_SEEK_FREQ_SPACING;
	u16 FM_SEEK_TUNE_SNR_THRESHOLD;
	u16 FM_SEEK_TUNE_RSSI_TRESHOLD;
	u16 RDS_INT_SOURCE;
	u16 RDS_INT_FIFO_COUNT;
	u16 RDS_CONFIG;
	u16 RX_VOLUME;
	u16 RX_HARD_MUTE;
} SI4705_PROP_CMD;

SI4705_PROP_CMD si4705_prop_cmd = {
	.GPO_IEN = 0x0001,
	.DIGITAL_OUTPUT_FORMAT = 0x0102,
	.DIGITAL_OUTPUT_SAMPLE_RATE = 0x0104,
	.REFCLK_FREQ = 0x0201,
	.REFCLK_PRESCALE = 0x0202,
	.FM_DEEMPHASIS = 0x1100,
	.FM_BLEND_STEREO_THRESHOLD = 0x1105,
	.FM_BLEND_MONO_THRESHOLD = 0x1106,
	.FM_ANTENNA_INPUT = 0x1107,
	.FM_MAX_TUNE_ERROR = 0x1108,
	.FM_RSQ_INT_SOURCE = 0x1200,
	.FM_RSQ_SNR_HI_THRESHOLD = 0x1201,
	.FM_RSQ_SNR_LO_THRESHOLD = 0x1202,
	.FM_RSQ_RSSI_HI_THRESHOLD = 0x1203,
	.FM_RSQ_RSSI_LO_THRESHOLD = 0x1204,
	.FM_RQS_BLEND_THRESHOLD = 0x1207,
	.FM_SOFT_MUTE_RATE = 0x1300,
	.FM_SOFT_MUTE_MAX_ATTENUATION = 0x1302,
	.FM_SOFT_MUTE_SNR_THRESHOLD = 0x1303,
	.FM_SEEK_BAND_BOTTOM = 0x1400,
	.FM_SEEK_BAND_TOP = 0x1401,
	.FM_SEEK_FREQ_SPACING = 0x1402,
	.FM_SEEK_TUNE_SNR_THRESHOLD = 0x1403,
	.FM_SEEK_TUNE_RSSI_TRESHOLD = 0x1404,
	.RDS_INT_SOURCE = 0x1500,
	.RDS_INT_FIFO_COUNT = 0x1501,
	.RDS_CONFIG = 0x1502,
	.RX_VOLUME = 0x4000,
	.RX_HARD_MUTE = 0x4001
};

#endif /* ifndef RADIO_SI4705_H */
